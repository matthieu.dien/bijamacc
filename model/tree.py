from model.node import Node


class Tree:

    def __init__(self, root: Node):
        self.root = root
        self.depth = root.getDepth()
        self.dyckWord = root.getDyckWord()
        self.index = 0
        self.setup_root()

    def copyTree(self):
        return Tree(self.root.copyNode())

    def addNode(self, index: int, nodeType: str):
        self.root.addNode(index, nodeType, self)
        self.depth = self.root.getDepth()
        self.dyckWord = self.root.getDyckWord()
        self.setup_root()

    def completeNode(self, index: int, nodeType: str):
        self.root.numberNodes()
        self.root.completeNode(index, nodeType, self)
        self.depth = self.root.getDepth()
        self.dyckWord = self.root.getDyckWord()
        self.setup_root()

    def setup_root(self):
        self.root.setup()
        self.root.add_offsets()



