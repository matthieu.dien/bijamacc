from random import randint

from model.level import Level
from model.levelEnum import getTreePlanarTreeToDyckWord
from model.node import putHole, binaryTreeToPlanarTree
from model.tree import Tree


def getIncompleteWord(dyckWord):
    nbOfHoles = randint(1, int(len(dyckWord) / 4))
    sizeOfHole = randint(int(1 / 3 * len(dyckWord) / nbOfHoles),
                         int(len(dyckWord) / nbOfHoles / 3))
    incompleteWord = dyckWord
    size = len(incompleteWord)
    min = int(size / nbOfHoles)
    max = min * nbOfHoles
    while min <= max:
        start = randint(min - int(size / nbOfHoles), min - sizeOfHole)
        incompleteWord = putHole(incompleteWord, start, start + sizeOfHole - 1)
        min += int(size / nbOfHoles)
    return incompleteWord


class LevelPlanarTreeToDyckWord(Level):
    def __init__(self, index):
        super().__init__(index)
        binaryTree = getTreePlanarTreeToDyckWord(index)
        self.tree = Tree(binaryTreeToPlanarTree(binaryTree.root))
        self.dyckWord = self.tree.dyckWord
        self.incompleteWord = getIncompleteWord(self.dyckWord)
        self.answer = ""
        self.score = None
        self.initialize()

    def initialize(self):
        self.score = 100
        self.answer = ""

    def nextLevel(self):
        return LevelPlanarTreeToDyckWord(self.index + 1)

    def completeWord(self, answers):
        answer = ""
        cptAnswers = 0
        for i in self.getSubWords():
            if i[0] == '?':
                answer += answers[cptAnswers]
                cptAnswers += 1
            else:
                answer += i
        self.answer = answer

    def result(self, answers):
        result = []
        index = 0
        cptAnswers = 0
        for i in self.getSubWords():
            if i[0] == '?':
                if answers[cptAnswers] == self.dyckWord[index: index + len(i)]:
                    result.append(True)
                else:
                    result.append(False)
                cptAnswers += 1
            index += len(i)
        return result

    def getSubWords(self):
        subWords = []
        subWordFound = False
        subWord = ""
        for i in self.incompleteWord:
            if i != '?':
                if subWordFound == False:
                    subWordFound = True
                    if subWord != "":
                        subWords.append(subWord)
                    subWord = "" + i

                else:
                    subWord += i
            else:
                if subWordFound == True:
                    subWordFound = False
                    subWords.append(subWord)
                    subWord = "" + i
                else:
                    subWord += i
        subWords.append(subWord)
        return subWords

    def finished(self):
        if self.answer == self.dyckWord:
            return True
        else:
            self.error()
            return False

