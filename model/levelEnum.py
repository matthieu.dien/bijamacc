import math
from enum import Enum

from model.node import BinaryNode, Leaf, randomTree
from model.tree import Tree


LevelsBinaryTreeToDyckWord = Enum("Levels", [
    ("Lvl1", Tree(BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf())))),
    ("Lvl2", Tree(BinaryNode(BinaryNode(Leaf(), Leaf()), BinaryNode(Leaf(), Leaf())))),
    ("Lvl3", Tree(BinaryNode(BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf())), BinaryNode(Leaf(), Leaf()))))])

LevelsDyckWordToBinaryTree = Enum("Levels", [
    ("Lvl1", Tree(BinaryNode(BinaryNode(Leaf(), Leaf()), Leaf()))),
    ("Lvl2", Tree(BinaryNode(BinaryNode(Leaf(), Leaf()), BinaryNode(Leaf(), Leaf())))),
    ("Lvl3", Tree(BinaryNode(BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf())), BinaryNode(Leaf(), Leaf()))))])

LevelsPlanarTreeToBinaryTree = Enum("Levels", [
    ("Lvl1", Tree(BinaryNode(Leaf(), BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf()))))),
    ("Lvl2", Tree(BinaryNode(BinaryNode(Leaf(), Leaf()), BinaryNode(Leaf(), Leaf())))),
    ("Lvl3", Tree(BinaryNode(BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf())), BinaryNode(Leaf(), Leaf()))))])

LevelsBinaryTreeToPlanarTree = Enum("Levels", [
    ("Lvl1", Tree(BinaryNode(BinaryNode(Leaf(), Leaf()), BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf()))))),
    ("Lvl2", Tree(BinaryNode(BinaryNode(Leaf(), Leaf()), BinaryNode(Leaf(), Leaf())))),
    ("Lvl3", Tree(BinaryNode(BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf())), BinaryNode(Leaf(), Leaf()))))])

LevelsPlanarTreeToDyckWord = Enum("Levels", [
    ("Lvl1", Tree(BinaryNode(Leaf(), BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf()))))),
    ("Lvl2", Tree(BinaryNode(BinaryNode(Leaf(), Leaf()), BinaryNode(Leaf(), Leaf())))),
    ("Lvl3", Tree(BinaryNode(BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf())), BinaryNode(Leaf(), Leaf()))))])

LevelsDyckWordToPlanarTree = Enum("Levels", [
    ("Lvl1", Tree(BinaryNode(Leaf(), BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf()))))),
    ("Lvl2", Tree(BinaryNode(BinaryNode(Leaf(), Leaf()), BinaryNode(Leaf(), Leaf())))),
    ("Lvl3", Tree(BinaryNode(BinaryNode(Leaf(), BinaryNode(Leaf(), Leaf())), BinaryNode(Leaf(), Leaf()))))])


def getTreeBinaryTreeToDyckWord(level):
    if level == 1:
        return LevelsBinaryTreeToDyckWord.Lvl1.value
    elif level == 2:
        return LevelsBinaryTreeToDyckWord.Lvl2.value
    elif level == 3:
        return LevelsBinaryTreeToDyckWord.Lvl3.value
    else:
        return Tree(randomTree(level + 1))

def getTreeDyckWordToBinaryTree(level):
    if level == 1:
        return LevelsDyckWordToBinaryTree.Lvl1.value
    elif level == 2:
        return LevelsDyckWordToBinaryTree.Lvl2.value
    elif level == 3:
        return LevelsDyckWordToBinaryTree.Lvl3.value
    else:
        return Tree(randomTree(level + 1))

def getTreePlanarTreeToBinaryTree(level):
    if level == 1:
        return LevelsPlanarTreeToBinaryTree.Lvl1.value
    elif level == 2:
        return LevelsPlanarTreeToBinaryTree.Lvl2.value
    elif level == 3:
        return LevelsPlanarTreeToBinaryTree.Lvl3.value
    else:
        return Tree(randomTree(10))

def getTreeBinaryTreeToPlanarTree(level):
    if level == 1:
        return LevelsBinaryTreeToPlanarTree.Lvl1.value
    elif level == 2:
        return LevelsBinaryTreeToPlanarTree.Lvl2.value
    elif level == 3:
        return LevelsBinaryTreeToPlanarTree.Lvl3.value
    else:
        return Tree(randomTree(level + 1))

def getTreePlanarTreeToDyckWord(level):
    if level == 1:
        return LevelsPlanarTreeToDyckWord.Lvl1.value
    elif level == 2:
        return LevelsPlanarTreeToDyckWord.Lvl2.value
    elif level == 3:
        return LevelsPlanarTreeToDyckWord.Lvl3.value
    else:
        return Tree(randomTree(10))

def getTreeDyckWordToPlanarTree(level):
    if level == 1:
        return LevelsDyckWordToPlanarTree.Lvl1.value
    elif level == 2:
        return LevelsDyckWordToPlanarTree.Lvl2.value
    elif level == 3:
        return LevelsDyckWordToPlanarTree.Lvl3.value
    else:
        return Tree(randomTree(level + 1))