from collections import defaultdict
from random import randint


def putHole(word, start, end):
    """
    this method permeates to create a hole in a dyck word, a part of the word is removed and replaced by some '?'

    :param word: the word which will have a hole
    :param start: the character index of the beginning of the hole
    :param end: the character index of the end of the hole
    :return: the word with the hole
    """
    incompleteWord = ""
    for i in range(len(word)):
        if start <= i <= end:
            incompleteWord += '?'
        else:
            incompleteWord += word[i]
    return incompleteWord


def cut(dyckWord):
    """
    this method is called when we create a tree from a dyck word,
    we need to cut the dyck word in 2 parts

    :param dyckWord: the dyck word we need to cut
    :return: the index corresponding with the caesura we will do in the word
    """
    cpt = 1
    parity = 1
    while parity > 0:
        if dyckWord[cpt] == ']':
            parity -= 1
        else:
            parity += 1
        cpt += 1
    return cpt - 1


def getNode(dyckWord):
    """
    this method permeates to get a binary tree from a dyck word

    :param dyckWord:th dyck word we use to get a tree
    :return: the binary tree corresponding with the dyck word
    """
    if len(dyckWord) == 0:
        return Leaf()
    else:
        c = cut(dyckWord)
        return BinaryNode(getNode(dyckWord[1: c]), getNode(dyckWord[c + 1: len(dyckWord)]))


def completeWord2(dyckWord, letters):
    """
    this method completes a word which has holes

    :param dyckWord: a dyck word with holes
    :param letters: a list of string, each string is a part of the word and will replace a hole
    :return:the dyck word completed
    """
    if len(letters) != dyckWord.count('?'):
        return "error"
    newWord = ""
    cpt = 0
    for i in dyckWord:
        if i != '?':
            newWord += i
        else:
            newWord += letters[cpt]
            cpt += 1
    return newWord


class Node:

    def setup(self, depth=0, places=None, offsets=None):
        pass

    def add_offsets(self, offset_sum=0):
        pass

    def getDepth(self):
        pass

    def getDyckWord(self):
        pass

    def copyNode(self):
        pass

    def addNode(self, index, nodeType, tree):
        pass

    def setParent(self, node, index):
        pass

    def isEqual(self, node):
        pass

    def checkNode(self, answer):
        pass

    def completeNode(self, index, nodeType, tree):
        pass

    def numberNodes(self):
        pass


class BinaryNode(Node):
    """
    this class represents a binary node with two children
    """

    def __init__(self, left, right):
        super(Node, self).__init__()
        self.left = left
        self.right = right
        # in a few funtions, we need that each node knows its parent
        self.left.setParent(self, 0)
        self.right.setParent(self, 1)
        self.parent = None
        # we need the position of the node compared with the others nodes of the tree to draw a tree
        self.x = 0
        self.y = 0
        self.height = 0
        self.width = 0
        self.offset = 0

    def setup(self, depth=0, places=None, offsets=None):
        """
        this method allows to give the position of the node compared with the others nodes of the tree in which it
        is part by filling the fields x, y and offset of the node

        :param depth: the depth of the node in the tree
        :param places:
        :param offsets:
        :return:
        """
        if places is None:
            places = defaultdict(lambda: 0)
        if offsets is None:
            offsets = defaultdict(lambda: 0)
        self.y = depth

        self.left.setup(depth + 1, places, offsets)
        self.right.setup(depth + 1, places, offsets)

        place = (self.left.x + self.right.x) / 2

        offsets[depth] = max(offsets[depth], places[depth] - place)

        self.x = place + offsets[depth]

        places[depth] = self.x + 1
        self.offset = offsets[depth]

    def add_offsets(self, offset_sum=0):
        """
        this is used to give the position of each node of the tree thanks to the offsets which are calculated with the
        method setup

        :param offset_sum:
        :return:
        """
        self.x += offset_sum
        offset_sum += self.offset
        self.height = self.y
        self.width = self.x
        self.left.add_offsets(offset_sum)
        self.height = max(self.left.height, self.height)
        self.width = max(self.left.width, self.width)
        self.right.add_offsets(offset_sum)
        self.height = max(self.right.height, self.height)
        self.width = max(self.right.width, self.width)

    def setParent(self, node, index):
        """
        this method gives a parent to this tree

        :param node: the parent of the tree
        :param index: the side this node is placed for his parent
        """
        self.parent = (node, index)

    def changeChild(self, node, index):
        """
        this method changes a child of the tree

        :param node: the new child of the tree
        :param index: the side of the new child
        """
        if index == 0:
            self.left = node
            node.setParent(self, 0)
        else:
            self.right = node
            node.setParent(self, 1)

    def getDyckWord(self):
        """
        this method return the dyck word corresponding with this tree

        :return: a string which is the dyck word corresponding with this tree
        """
        arbre = "["
        arbre += self.left.getDyckWord()
        arbre += "]"
        arbre += self.right.getDyckWord()
        return arbre

    def getDepth(self):
        """
        this method return the depth of the tree

        :return: an int corresponding of the depht of the tree
        """
        return 1 + max(self.left.getDepth(), self.right.getDepth())

    def treeChecked(self, dyckWord):
        """
        this method check if the tree correspond to the dyck word in parameter

        :param dyckWord: the dyckword we check
        :return: True if it's ok, False otherwise
        """
        return dyckWord == self.getDyckWord()

    def isEqual(self, node):
        """
        this method is used to compare two nodes and to know if they are the same,
        the node have to be ended, that's to say they don't contain EmptyNode

        :param node: the node we compare with
        :return: True if the nodes are the sames, False otherwise
        """
        if isinstance(node, EmptyNode):
            return False
        elif isinstance(self, Leaf):
            if isinstance(node, Leaf):
                return True
            else:
                return False
        else:
            if isinstance(node, Leaf):
                return False
            else:
                return self.left.isEqual(node.left) and self.right.isEqual(node.right)

    def checkNode(self, answer):
        """
        this method is used to compare this node with another which is not necessarily ended, that's to say it
        can contain an EmptyNode
        In this method, an EmptyNode is considered as the same as a node or a leaf

        :param answer: the node we wan to compare with
        :return: True if the nodes are the sames, False otherwise
        """
        if isinstance(answer, EmptyNode):
            return True
        elif isinstance(answer, Leaf) and isinstance(self, Leaf):
            return True
        elif isinstance(answer, Leaf) and not isinstance(self, Leaf):
            return False
        elif isinstance(self, Leaf):
            return False
        else:
            return self.left.checkNode(answer.left) and self.right.checkNode(answer.right)

    def checkNodeBis(self, answer):
        if isinstance(answer, Leaf):
            return True
        else:
            return self.left.checkNodeBis(answer.left) and self.right.checkNodeBis(answer.right)

    def addNode(self, index: int, nodeType: str, tree):
        """
        this method which allows to add a node child to the node
        is will add the node of type 'nodeType' at the position 'index'
        the index is the number of the EmptyNode that we will replace in the node by a new node of type
        Leaf or BinaryNode
        the EmptyNode are numbered in the ascending order that they are encountered in the prefix path of the node

        :param nodeType: a string which indicates if we will add a node or a leaf
        :param index: the number of the EmptyNode that we will replace in the node by a new node of type Leaf or BinaryNode
        the EmptyNode are numbered in the ascending order that they are encountered in the prefix path of the node
        :return: the new index (when we meet an EmptyNode, we decrements the index)
        """
        return self.right.addNode(self.left.addNode(index, nodeType, tree), nodeType, tree)

    def getBrothers(self):
        brothers = [self.binaryToPlanarRec()]
        brother = self.right
        while isinstance(brother, BinaryNode):
            brothers.append(brother.binaryToPlanarRec())
            brother = brother.right
        return brothers

    def toStr(self, tab=0):
        return ' ' * tab + 'NODE' + '\n' + "".join(self.left.toStr(tab + 2)) + "".join(self.right.toStr(tab + 2))

    def __str__(self):
        return self.toStr()

    def copyNode(self):
        """
        this method returns a copy of itself

        :return: a copy of itself
        """

        return BinaryNode(self.left.copyNode(), self.right.copyNode())


class PlanarNode(Node):
    """
    this class represents a planar node with several children
    """

    def __init__(self, children: list):
        super(Node, self).__init__()
        self.children = children
        self.x = 0
        self.y = 0
        self.height = 0
        self.width = 0
        self.offset = 0
        self.parent = None
        self.index = None
        for i in range(len(self.children)):
            self.children[i].setParent(self, i)

    def setParent(self, node, index):
        """
        this method gives a parent to this tree

        :param node: the parent of the tree
        :param index: the side this node is placed for his parent
        """
        self.parent = (node, index)

    def addChild(self, node):
        self.children.append(node)
        node.setParent(self, len(self.children) - 1)

    def changeChild(self, node, index: int):
        self.children[index] = node

    def numberNodes(self, index=0, brothers=None):
        """
        this method is used to number each node of the tree with a prefixed route

        :param index: the number of the next node of the tree
        :param brothers: a list of the next nodes which will be numbered
        """
        if brothers is None:
            brothers = []
        if not self.children:
            if brothers:
                brothers.pop(0).numberNodes(index, brothers)
        else:
            newBrothers = self.children[1:] + brothers
            self.children[0].numberNodes(index, newBrothers)

    def completeNode(self, index: int, nodeType: str, tree):

        for child in self.children:
            child.completeNode(index, nodeType, tree)

    def checkNode(self, node, nodeBrothers=None, brothers=None):
        if brothers is None:
            brothers = []
        if nodeBrothers is None:
            nodeBrothers = []

        # the node can't have more children than the model
        if isinstance(node, IncompleteNode):
            if len(self.children) < len(node.children):
                return False
        # moreover, if the node is completed, it should have exactly the same number of children than the model
        else:
            if len(self.children) != len(node.children):
                return False

        if not self.children:
            # if they have not children, call this funtion with the brother of the current nodes
            if brothers:
                return brothers.pop(0).checkNode(nodeBrothers.pop(0), nodeBrothers, brothers)
            else:
                # if they have no children and no brothers, they are exactly the same trees
                return True
        else:
            # if the model has children but the node has not, we call the funtion with the brothers
            if not node.children:
                if brothers:
                    if not nodeBrothers:
                        return True
                    else:
                        return brothers.pop(0).checkNode(nodeBrothers.pop(0), nodeBrothers, brothers)
                else:
                    return True
            else:
                # we add the brothers of the model and the node in the lists, for the models, we only add the brothers
                # which are represented in the node
                newNodeBrothers = node.children[1:] + nodeBrothers
                if len(self.children) == len(node.children):
                    newBrothers = self.children[1:] + brothers
                else:
                    tmp = []
                    if len(node.children) > 2:
                        for i in range(1, len(node.children[1:])):
                            print(self.children[i])
                            tmp.append(self.children[i])
                    elif len(node.children) == 2:
                        tmp.append(self.children[1])
                    newBrothers = tmp + brothers
                return self.children[0].checkNode(node.children[0], newNodeBrothers, newBrothers)

    def isEqual(self, node, nodeBrothers=None, brothers=None):
        if brothers is None:
            brothers = []
        if nodeBrothers is None:
            nodeBrothers = []
        if isinstance(node, IncompleteNode):
            return False
        else:
            if not self.children:
                if node.children:
                    return False
                else:
                    if brothers:
                        if not nodeBrothers:
                            return False
                        else:
                            return brothers.pop(0).isEqual(nodeBrothers.pop(0), nodeBrothers, brothers)
                    else:
                        if nodeBrothers:
                            return False
                        else:
                            return True
            else:
                if len(self.children) == len(node.children):
                    newBrothers = self.children[1:] + brothers
                    newNodeBrothers = node.children[1:] + nodeBrothers
                    if len(newNodeBrothers) != len(newBrothers):
                        return False
                    else:
                        return self.children[0].isEqual(node.children[0], newNodeBrothers, newBrothers)
                else:
                    return False

    def setup(self, depth=0, places=None, offsets=None):
        nb_children = len(self.children)
        if places is None:
            places = defaultdict(lambda: 0)
        if offsets is None:
            offsets = defaultdict(lambda: 0)
        self.y = depth

        for child in self.children:
            child.setup(depth + 1, places, offsets)

        if nb_children == 0:
            place = places[depth]
            self.x = place
        else:
            place = (self.children[0].x + self.children[nb_children - 1].x) / 2

        offsets[depth] = max(offsets[depth], places[depth] - place)

        if nb_children != 0:
            self.x = place + offsets[depth]

        places[depth] = self.x + 1
        self.offset = offsets[depth]

    def add_offsets(self, offset_sum=0):
        self.x += offset_sum
        offset_sum += self.offset
        self.height = self.y
        self.width = self.x
        for child in self.children:
            child.add_offsets(offset_sum)
            self.height = max(self.height, child.height)
            self.width = max(self.width, child.width)

    def getDyckWord(self):
        binaryNode = planarTreeToBinaryTree(self)
        return binaryNode.getDyckWord()

    def copyNode(self):
        copyChildren = []
        for child in self.children:
            copyChildren.append(child.copyNode())
        return PlanarNode(copyChildren)

    def getDepth(self):
        """
        this method return the depht of the tree

        :return: an int corresponding of the depht of the tree
        """
        if not self.children:
            return 0
        else:
            return 1 + max(child.getDepth() for child in self.children)

    def toStr(self, tab=0):
        return ' ' * tab + 'PNODE' + '\n' + "".join([child.toStr(tab + 2) for child in self.children])

    def __str__(self):
        return self.toStr()


class IncompleteNode(PlanarNode):
    """
    this class represents a planarNode which has not all its children
    """
    def __init__(self, children):
        super(PlanarNode, self).__init__()
        self.children = children
        self.x = 0
        self.y = 0
        self.height = 0
        self.width = 0
        self.offset = 0
        self.parent = None
        self.index = None

        for i in range(len(self.children)):
            self.children[i].setParent(self, i)

    def copyNode(self):
        copyChildren = []
        for child in self.children:
            copyChildren.append(child.copyNode())
        return IncompleteNode(copyChildren)

    def setParent(self, node, index):
        """
        this method gives a parent to this tree

        :param node: the parent of the tree
        :param index: the side this node is placed for his parent
        """
        self.parent = (node, index)

    def numberNodes(self, index=0, brothers=None):
        """
        this method is used to number each node of the tree with a prefixed route

        :param index: the number of the next node of the tree
        :param brothers: a list of the next nodes which will be numbered
        """
        if brothers is None:
            brothers = []
        self.index = index
        if not self.children:
            if brothers:
                brothers.pop(0).numberNodes(index + 1, brothers)
        else:
            newBrothers = self.children[1:] + brothers
            self.children[0].numberNodes(index + 1, newBrothers)

    def completeNode(self, index: int, nodeType: str, tree):
        """
        this method allows to complete a tree which has at least 1 incomplete node
        :param index: the index of the incomplete node that will be completed
        :param nodeType: the type of completion, if it is "branch", a child we be added to the incomplete node,
        otherwise, the incomplete node will become a planar node (a complete node)
        :param tree: the tree on which an incomplete node will be completed
        """
        if index == self.index:
            if nodeType == "branch":
                self.addChild(IncompleteNode([]))
            else:
                if self.parent is not None:
                    completedNode = PlanarNode(self.children)
                    self.parent[0].changeChild(completedNode, self.parent[1])
                else:
                    tree.root = PlanarNode(self.children)
        else:
            for child in self.children:
                child.completeNode(index, nodeType, tree)

    def isEqual(self, node, nodeBrothers=None, brothers=None):
        if brothers is None:
            brothers = []
        if nodeBrothers is None:
            nodeBrothers = []
        if not isinstance(node, IncompleteNode):
            return False
        else:
            if not self.children:
                if node.children:
                    return False
                else:
                    if brothers:
                        if not nodeBrothers:
                            return False
                        else:
                            return brothers.pop(0).checkNode(nodeBrothers.pop(0), nodeBrothers, brothers)
                    else:
                        if nodeBrothers:
                            return False
                        else:
                            return True
            else:
                if len(self.children) == len(node.children):
                    newBrothers = self.children[1:] + brothers
                    newNodeBrothers = node.children[1:] + nodeBrothers
                    if len(newNodeBrothers) != len(newBrothers):
                        return False
                    else:
                        return self.children[0].numberNodes(node.children[0], newNodeBrothers, newBrothers)
                else:
                    return False

    def addChild(self, node):
        self.children.append(node)
        node.setParent(self, len(self.children) - 1)

    def toStr(self, tab=0):
        return ' ' * tab + 'INCOMPLETE NODE' + " " + str(self.incompleteNodeIndex) + '\n' + "".join(
            [child.toStr(tab + 2) for child in self.children])

    def __str__(self):
        return self.toStr()


class Leaf(BinaryNode):
    """
    this class inherits from Node, actually, a Leaf is a Node which has no children
    """

    def __init__(self):
        super(BinaryNode, self).__init__()
        self.x = 0
        self.y = 0
        self.height = 0
        self.width = 0
        self.offset = 0

    def copyNode(self):
        return Leaf()

    def addNode(self, index: int, nodeType: str, tree):
        return index

    def setup(self, depth=0, places=None, offsets=None):
        if places is None:
            places = defaultdict(lambda: 0)
        if offsets is None:
            offsets = defaultdict(lambda: 0)
        self.y = depth

        place = places[depth]
        self.x = place

        offsets[depth] = max(offsets[depth], places[depth] - place)

        places[depth] = self.x + 1
        self.offset = offsets[depth]

    def add_offsets(self, offset_sum=0):
        self.x += offset_sum
        offset_sum += self.offset
        self.height = self.y
        self.width = self.x

    def checkNodeBis(self, answer):
        if not isinstance(answer, Leaf):
            return False
        else:
            return True

    def getDyckWord(self):
        return ""

    def getDepth(self):
        return 0

    def getBrothers(self):
        return []

    def __str__(self):
        return self.toStr()

    def toStr(self, tab=0):
        return ' ' * tab + 'LEAF' + '\n'


class EmptyNode(Node):
    """
    this class represents a node of a binary tree which is incomplete, it has no children and it is intended to become a BinaryNode
    or a leaf
    """
    def __init__(self):
        super(Node, self).__init__()
        self.x = 0
        self.y = 0
        self.height = 0
        self.width = 0
        self.offset = 0
        self.parent = None

    def copyNode(self):
        return EmptyNode()

    def isEqual(self, node):
        return False

    def checkNode(self, answer):
        return False

    def setParent(self, node, index):
        """
        this method gives a parent to this tree

        :param node: the parent of the tree
        :param index: the side this node is placed for his parent
        """
        self.parent = (node, index)

    def addNode(self, index: int, nodeType: str, tree):
        if index == 0:
            if nodeType == "node":
                if self.parent is not None:
                    self.parent[0].changeChild(BinaryNode(EmptyNode(), EmptyNode()), self.parent[1])
                else:
                    tree.root = BinaryNode(EmptyNode(), EmptyNode())
            else:
                if self.parent is not None:
                    self.parent[0].changeChild(Leaf(), self.parent[1])
                else:
                    tree.root = Leaf()
            return -1
        else:
            return index - 1

    def getBrothers(self):
        return []

    def setup(self, depth=0, places=None, offsets=None):
        if places is None:
            places = defaultdict(lambda: 0)
        if offsets is None:
            offsets = defaultdict(lambda: 0)
        self.y = depth

        place = places[depth]
        self.x = place

        offsets[depth] = max(offsets[depth], places[depth] - place)

        places[depth] = self.x + 1
        self.offset = offsets[depth]

    def add_offsets(self, offset_sum=0):
        self.x += offset_sum
        offset_sum += self.offset
        self.height = self.y
        self.width = self.x

    def getDyckWord(self):
        return ""

    def getDepth(self):
        return 0

    def __str__(self):
        return self.toStr()

    def toStr(self, tab=0):
        return ' ' * tab + 'EMPTY' + '\n'


def randomTree(nbInternNodes):
    """
    This method generate a random binary tree with the number of intern nodes nbNodes

    :param nbInternNodes: the number of intern nodes of the tree the method will create
    :return: a random binary tree
    """
    n = BinaryNode(Leaf(), Leaf())
    nodeList = [n, n.left, n.right]
    nbNodeTotal = 2
    root = 0
    for i in range(nbInternNodes - 1):
        # we randomly choose a node in the current tree, we will place a new node at its place
        # and the chosen node will become one of the children of this new node
        nodeIndex = randint(0, nbNodeTotal)
        chosenNode = nodeList[nodeIndex]
        if root != nodeIndex:
            parent = chosenNode.parent
        # we randomly choose a side: left or right
        # if we choose right, we will place the chosen node as the right child of the new node and at left, we will
        # place a leaf and vice versa
        if randint(0, 1) == 0:
            n = BinaryNode(chosenNode, Leaf())
            nodeList.append(n)
            nodeList.append(n.right)
        else:
            n = BinaryNode(Leaf(), chosenNode)
            nodeList.append(n)
            nodeList.append(n.left)
        # we place the new node as the child of the former parent of the node we chose at beginning
        if root != nodeIndex:
            parent[0].changeChild(nodeList[nbNodeTotal + 1], parent[1])
        else:
            root = nbNodeTotal + 1
        nbNodeTotal += 2
    return nodeList[root]


def catalan(n):
    """
    return the number of catalan sequence with the index n

    :param n: the index of the number in the catalan sequence we will return
    :return: the number of catalan sequence with the index n
    """
    cn = 1
    a = 1
    b = 1
    for i in range(1, 2 * n + 1):
        cn *= i
    for i in range(1, n + 2):
        a *= i
    for i in range(1, n + 1):
        b *= i
    return cn / (a * b)


def testRandomTreeGeneration(nbTests, treeMin, treeMax):
    """
    this methodmethod tests if the method which generates random trees is works well, that's to say if all trees can be generating
    uniformly by the method

    :param nbTests: number of trees we generate for each size of tree
    :param treeMin: the minimum size of the trees we will generate
    :param treeMax: the maximum size of the trees we will generate
    """
    d = dict()
    testLongueur = True
    testNbArbres = True
    res = ""
    for i in range(treeMin, treeMax + 1):
        for j in range(nbTests):
            a = randomTree(i).getDyckWord()
            if a in d:
                d[a] += 1
            else:
                d[a] = 1
        for k in d.keys():
            res += k + ": " + str(d[k] * 100 / nbTests) + "%  |  "
            if len(k) != 2 * i:
                testLongueur = False
        if len(d) != catalan(i):
            testNbArbres = False

        print("longueur: " + str(i))
        print("test longueur: " + str(testLongueur))
        print("test nombre d'abres: " + str(testNbArbres))
        print("résultat: " + res)
        print(d)
        print("\n \n")
        testLongueur = True
        testNbArbres = True
        d = dict()
        res = ""


def binaryTreeToPlanarTree(binaryTree):
    """
    this method change a binaryTree into a PlanarTree
    """
    return binaryToPlanarRec(BinaryNode(binaryTree, Leaf()))


def binaryToPlanarRec(binaryTree):
    if not isinstance(binaryTree, Leaf):
        return PlanarNode(getBrothers(binaryTree.left))


def getBrothers(binaryNode: BinaryNode):
    if isinstance(binaryNode, Leaf):
        return []
    else:
        brothers = [binaryToPlanarRec(binaryNode)]
        brother = binaryNode.right
        while not isinstance(brother, Leaf):
            brothers.append(binaryToPlanarRec(brother))
            brother = brother.right
        return brothers


def planarTreeToBinaryTree(planarTree):
    """
    this method change a planar tree in a binary tree
    """
    if planarTree.children:
        return planarToBinaryRec(planarTree.children[0], planarTree.children[1:])
    else:
        return Leaf()


def planarToBinaryRec(planarTree, brothers):
    """
    this method is called recursively to get a binary tree from a planr tree

    :param planarTree: the planar tree
    :param brothers: the brothers of the parent of the planar tree
    :return: a binary tree corresponding with the planar tree
    """
    if planarTree.children:
        left = planarToBinaryRec(planarTree.children[0], planarTree.children[1:])
    else:
        left = Leaf()
    if brothers:
        right = planarToBinaryRec(brothers[0], brothers[1:])
    else:
        right = Leaf()
    return BinaryNode(left, right)

