from model.level import Level
from model.levelEnum import getTreeBinaryTreeToDyckWord, getTreePlanarTreeToBinaryTree
from model.node import BinaryNode, EmptyNode, binaryTreeToPlanarTree
from model.tree import Tree


class LevelPlanarTreeToBinaryTree(Level):
    def __init__(self, index):
        super().__init__(index)
        self.binaryAnswer = getTreePlanarTreeToBinaryTree(index)
        self.answer = Tree(binaryTreeToPlanarTree(self.binaryAnswer.root))
        self.tree = None
        self.score = None
        self.initialize()

    def initialize(self):
        self.score = 100
        self.tree = Tree(EmptyNode())

    def nextLevel(self):
        return LevelPlanarTreeToBinaryTree(self.index + 1)

    def addNode(self, index: int, nodeType: str):
        self.tree.root.addNode(index, nodeType)

    def checkTree(self, index: int, nodeType: str):
        copy = self.tree.copyTree()
        copy.addNode(index, nodeType)
        if self.binaryAnswer.root.checkNode(copy.root):
            self.tree = copy
            return True
        else:
            return False

    def finished(self):
        return self.tree.root.isEqual(self.binaryAnswer.root)
