from model.level import Level
from model.levelEnum import getTreeBinaryTreeToDyckWord, getTreeBinaryTreeToPlanarTree
from model.node import BinaryNode, EmptyNode, PlanarNode, IncompleteNode, binaryTreeToPlanarTree
from model.tree import Tree


class LevelBinaryTreeToPlanarTree(Level):
    def __init__(self, index):
        super().__init__(index)
        self.answer = getTreeBinaryTreeToPlanarTree(index)
        self.planarAnswer = Tree(binaryTreeToPlanarTree(self.answer.root))
        self.tree = None
        self.score = None
        self.initialize()

    def initialize(self):
        self.score = 100
        self.tree = Tree(IncompleteNode([]))

    def nextLevel(self):
        return LevelBinaryTreeToPlanarTree(self.index + 1)

    def addNode(self, index: int, nodeType: str):
        self.tree.root.addNode(index, nodeType)

    def checkTree(self, index: int, nodeType: str):
        copy = self.tree.copyTree()
        copy.completeNode(index, nodeType)
        if self.planarAnswer.root.checkNode(copy.root):
            self.tree.completeNode(index, nodeType)
            return True
        else:
            return False

    def finished(self):
        return self.planarAnswer.root.isEqual(self.tree.root)
