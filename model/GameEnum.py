from enum import Enum


def nbGames():
    return GameEnum.__len__()

class GameEnum(Enum):
    BINARY_TREE_TO_DYCK_WORD = 0
    DYCK_WORD_TO_BINARY_TREE = 1
    BINARY_TREE_TO_PLANAR_TREE = 2
    PLANAR_TREE_TO_BINARY_TREE = 3
    DYCK_WORD_TO_PLANAR_TREE = 4
    PLANAR_TREE_TO_DYCK_WORD = 5

