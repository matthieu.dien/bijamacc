from model.level import Level
from model.levelEnum import getTreeDyckWordToPlanarTree
from model.node import IncompleteNode, binaryTreeToPlanarTree
from model.tree import Tree


class LevelDyckWordToPlanarTree(Level):
    def __init__(self, index):
        super().__init__(index)
        self.answer = getTreeDyckWordToPlanarTree(index)
        self.planarAnswer = Tree(binaryTreeToPlanarTree(self.answer.root))
        self.tree = None
        self.score = None
        self.initialize()

    def nextLevel(self):
        return LevelDyckWordToPlanarTree(self.index + 1)

    def addNode(self, index: int, nodeType: str):
        self.tree.root.addNode(index, nodeType)

    def checkTree(self, index: int, nodeType: str):
        copy = self.tree.copyTree()
        copy.completeNode(index, nodeType)
        if self.planarAnswer.root.checkNode(copy.root):
            self.tree.completeNode(index, nodeType)
            return True
        else:
            return False

    def initialize(self):
        self.score = 100
        self.tree = Tree(IncompleteNode([]))

    def finished(self):
        return self.planarAnswer.root.isEqual(self.tree.root)