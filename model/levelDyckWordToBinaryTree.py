from model.level import Level
from model.levelEnum import getTreeBinaryTreeToDyckWord, getTreeDyckWordToBinaryTree
from model.node import BinaryNode, EmptyNode
from model.tree import Tree


class LevelDyckWordToBinaryTree(Level):
    def __init__(self, index):
        super().__init__(index)
        self.answer = getTreeDyckWordToBinaryTree(index)
        self.tree = None
        self.score = None
        self.initialize()

    def nextLevel(self):
        return LevelDyckWordToBinaryTree(self.index + 1)

    def addNode(self, index: int, nodeType: str):
        self.tree.root.addNode(index, nodeType)

    def checkTree(self, index: int, nodeType: str):
        copy = self.tree.copyTree()
        copy.addNode(index, nodeType)
        if self.answer.root.checkNode(copy.root):
            self.tree.addNode(index, nodeType)
            return True
        else:
            return False

    def initialize(self):
        self.score = 100
        self.tree = Tree(BinaryNode(EmptyNode(), EmptyNode()))

    def finished(self):
        return self.tree.root.isEqual(self.answer.root)
