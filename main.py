# an application is a window which contains a central layout and a view
import sys

from PyQt5.QtWidgets import QApplication

from controller.menuController import MenuController
from model.level import Level
from view.menu import Menu


def launchApplication():
    qapp = QApplication(sys.argv)
    controller = MenuController()
    controller.launchApplication()
    qapp.exec_()


if __name__ == '__main__':
    launchApplication()

