from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt

from view.levelGui import LevelGui
from view.treeView import TreeView


class PlanarTreeToDyckWordGui(LevelGui):
    def __init__(self, controller):
        super().__init__(controller)

        self.createDyckWordGroupBox()
        self.createButtons()
        self.createTreeView()

        self.layout = QGridLayout()
        self.layout.addLayout(self.menuBarLayout, 0, 0, 1, 2)
        self.layout.addWidget(self.treeView, 1, 0)
        self.layout.addWidget(self.dyckWordGroupBox, 1, 1)
        self.layout.setColumnStretch(0, 2)
        self.layout.setColumnStretch(1, 1)

        self.setLayout(self.layout)

    def createDyckWordGroupBox(self):
        self.dyckWordGroupBox = QGroupBox()
        self.dyckWordGroupBox.setStyleSheet("background: white; border: 1px")
        self.globalDyckWordLayout = QVBoxLayout()
        self.dyckWordLayout = QHBoxLayout()
        self.globalDyckWordLayout.addLayout(self.dyckWordLayout, 1)
        self.dyckWordGroupBox.setLayout(self.globalDyckWordLayout)
        self.dyckWordLayout.addStretch(1)

        self.indexHole = 1
        self.indexSubWord = 0
        for subWord in self.controller.level.getSubWords():
            if subWord[0] == '?':
                self.displayHole(subWord)
                self.indexHole += 1
            else:
                self.displayWord(subWord)
            self.indexSubWord += 1

        self.dyckWordLayout.addStretch(1)

    def displayWord(self, word):
        newWord = ""
        for i in range(len(word)):
            newWord += word[i]
            if i != len(word) - 1:
                newWord += " "
        label = QLabel(newWord)
        label.setFont(QFont("Arial", 25))
        self.dyckWordLayout.addWidget(label)

    def displayHole(self, hole):
        fontSize = 20
        font = QFont("Arial", fontSize)
        if self.indexHole == 1:
            self.answer1 = QLineEdit()
            self.answer1.setStyleSheet("border: 3px solid black")
            self.answer1.setPlaceholderText(hole)
            self.answer1.setFont(font)
            self.answer1.setFixedWidth(6 + len(hole) * fontSize)
            self.answer1.setMaxLength(len(hole))
            self.dyckWordLayout.addWidget(self.answer1)
        elif self.indexHole == 2:
            self.answer2 = QLineEdit()
            self.answer2.setStyleSheet("border: 3px solid black")
            self.answer2.setPlaceholderText(hole)
            self.answer2.setFont(font)
            self.answer2.setMaxLength(len(hole))
            self.answer2.setMaximumWidth(6 + len(hole) * fontSize)
            self.dyckWordLayout.addWidget(self.answer2)
        else:
            self.answer3 = QLineEdit()
            self.answer3.setStyleSheet("border: 3px solid black")
            self.answer3.setPlaceholderText(hole)
            self.answer3.setMaxLength(len(hole))
            self.answer3.setFont(font)
            self.answer3.setMaximumWidth(6 + len(hole) * fontSize)
            self.dyckWordLayout.addWidget(self.answer3)

    def createButtons(self):
        self.resultLabel = QLabel()
        self.resultLabel.setAlignment(Qt.AlignCenter)
        self.globalDyckWordLayout.addWidget(self.resultLabel)
        self.submit = QPushButton("Valider")
        self.submit.setShortcut("Return")
        self.submit.clicked.connect(self.result)
        submitLayout = QHBoxLayout()
        submitLayout.addStretch(1)
        submitLayout.addWidget(self.submit)
        self.globalDyckWordLayout.addLayout(submitLayout)

    def result(self):
        answers = [self.answer1.text()]
        if self.indexHole >= 3:
            answers.append(self.answer2.text())
        if self.indexHole >= 4:
            answers.append(self.answer3.text())
        self.controller.level.completeWord(answers)
        self.displayResult(answers)
        if self.controller.level.finished():
            self.finish()
        else:
            self.changeScore()
            self.resultLabel.setText("Essaie encore")

    def displayResult(self, answers):
        result = self.controller.level.result(answers)
        if result[0]:
            self.answer1.setStyleSheet("border: 3px solid green")
        else:
            self.answer1.setStyleSheet("border: 3px solid red")
        if len(result) > 1:
            if result[1]:
                self.answer2.setStyleSheet("border: 3px solid green")
            else:
                self.answer2.setStyleSheet("border: 3px solid red")
        if len(result) > 2:
            if result[2]:
                self.answer3.setStyleSheet("border: 3px solid green")
            else:
                self.answer3.setStyleSheet("border: 3px solid red")

    def createTreeView(self):
        self.treeScene = QGraphicsScene()
        self.treeView = TreeView(self.treeScene, controller=self.controller)
        self.treeView.drawTree()
