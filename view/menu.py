import sys
from math import sqrt

from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import *

from model.GameEnum import nbGames, GameEnum
from view.gameButton import GameButton
from view.levelButton import LevelButton
from view.menuButton import MenuButton
from view.menuWidget import MenuWidget


class MyWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(QMainWindow, self).__init__(*args, **kwargs)

    def resizeEvent(self, e):
        self.setMaximumHeight(int(0.7 * self.width()))


class Menu:
    def __init__(self, controller):
        self.controller = controller
        self.launcher = None
        self.levelGui = None
        self.levelLayout = None
        self.menuWidget = None
        self.window = MyWindow(None)
        self.menuAction = QAction()
        self.gameButtons = []
        self.window.setMinimumSize(1000, 500)
        self.window.setCentralWidget(QWidget())
        self.window.centralWidget().setStyleSheet("background: #F2F3F4")
        self.levelButtons = []
        self.centralLayout = QHBoxLayout(self.window.centralWidget())
        self.paramWindow(1200, 600, "Bijamacc")
        self.displayGames()
        self.shortCutAction = QAction()
        self.shortCutAction.setShortcut("Ctrl+W")
        self.shortCutAction.triggered.connect(self.window.close)
        self.window.addAction(self.shortCutAction)


    # function to change parameters of the window (size, title, color...)
    def paramWindow(self, width, high, title):
        self.window.resize(width, high)
        self.window.setWindowTitle(title)

    """def displayGames(self):
        self.removeCentralLayout()
        self.menuWidget = QWidget()
        self.gamesLayout = QGridLayout()
        cptRow = 0
        for i in range(nbGames()):
            game = GameEnum(i)
            buttonGame = GameButton(game)
            self.gameButtons.append(buttonGame)
            if i < 3:
                self.gamesLayout.addWidget(buttonGame, cptRow, 0)
            else:
                self.gamesLayout.addWidget(buttonGame, cptRow, 1)
            if i == 2:
                cptRow = 0
            else:
                cptRow += 1


        self.menuWidget.setLayout(self.gamesLayout)
        self.centralLayout.addWidget(self.menuWidget)"""

    def displayGames(self):
        self.removeCentralLayout()
        self.menuWidget = QWidget()
        widget = MenuWidget(self.controller)
        self.gamesLayout = QGridLayout()
        self.gamesLayout.addWidget(widget, 0, 0, 5, 10)

        planarTreeToBinaryTreeButton = GameButton(GameEnum.PLANAR_TREE_TO_BINARY_TREE)
        self.gameButtons.append(planarTreeToBinaryTreeButton)
        self.gamesLayout.addWidget(planarTreeToBinaryTreeButton, 0, 0, 3, 4)

        planarTreeToBinaryTreeButton = GameButton(GameEnum.BINARY_TREE_TO_PLANAR_TREE)
        self.gameButtons.append(planarTreeToBinaryTreeButton)
        self.gamesLayout.addWidget(planarTreeToBinaryTreeButton, 0, 4, 3, 1)

        dyckWordToBinaryTreeButton = GameButton(GameEnum.DYCK_WORD_TO_BINARY_TREE)
        self.gameButtons.append(dyckWordToBinaryTreeButton)
        self.gamesLayout.addWidget(dyckWordToBinaryTreeButton, 0, 5, 3, 1)

        binaryTreeToDyckWordButton = GameButton(GameEnum.BINARY_TREE_TO_DYCK_WORD)
        self.gameButtons.append(binaryTreeToDyckWordButton)
        self.gamesLayout.addWidget(binaryTreeToDyckWordButton, 0, 6, 3, 4)

        planarTreeToDyckWordButton = GameButton(GameEnum.PLANAR_TREE_TO_DYCK_WORD)
        self.gameButtons.append(planarTreeToDyckWordButton)
        self.gamesLayout.addWidget(planarTreeToDyckWordButton, 3, 0, 1, 10)

        dyckWordToPlanarTreeButton = GameButton(GameEnum.DYCK_WORD_TO_PLANAR_TREE)
        self.gameButtons.append(dyckWordToPlanarTreeButton)
        self.gamesLayout.addWidget(dyckWordToPlanarTreeButton, 4, 0, 1, 10)

        self.menuWidget.setLayout(self.gamesLayout)
        self.centralLayout.addWidget(self.menuWidget)

    def displayLevels(self, nbLevels, levelController):
            self.removeCentralLayout()
            self.menuWidget = QWidget()
            self.levelLayout = QGridLayout()
            menuButton = MenuButton(QColor(52, 73, 94))
            menuButton.clickableObject.clicked.connect(levelController.menuController.displayMenu)
            self.levelLayout.addWidget(menuButton, 0, 0)
            for i in range(nbLevels):
                levelButton = LevelButton(i + 1, levelController)
                self.levelButtons.append(levelButton)
                self.levelLayout.addWidget(levelButton, 1, i)
            infinityButton = LevelButton(-1, levelController)
            self.levelLayout.addWidget(infinityButton, 2, 0, 1, nbLevels)

            self.menuWidget.setLayout(self.levelLayout)
            self.centralLayout.addWidget(self.menuWidget)

    def displayLevel(self, levelGui):
        self.removeCentralLayout()
        self.levelGui = levelGui
        self.centralLayout.addWidget(levelGui)


    def displayLauncher(self, launcher: QWidget):
        self.removeCentralLayout()
        self.launcher = launcher
        self.centralLayout.addWidget(self.launcher)

    def removeCentralLayout(self):
        if self.menuWidget is not None:
            self.centralLayout.removeWidget(self.menuWidget)
            self.menuWidget.deleteLater()
            self.menuWidget = None
        if self.levelGui is not None:
            self.centralLayout.removeWidget(self.levelGui)
            self.levelGui.deleteLater()
            self.levelGui = None
        if self.launcher is not None:
            self.centralLayout.removeWidget(self.launcher)
            self.launcher.deleteLater()
            self.launcher = None




if __name__ == '__main__':
    qapp = QApplication(sys.argv)
    app = Menu()
    app.paramWindow(1200, 600, "Initiation au parcours d'arbre")
    app.addSubMenus("&Jeu", {""""&Nouvelle partie": app.launchGame,""" "&Niveaux": None, "&Quitter": None})
    app.addSubMenus("&Aide", {"&Aide": None})
    app.addSubMenus("&A propos", {"&A propos": None})
    app.displayGames()
    app.window.show()
    qapp.exec_()
