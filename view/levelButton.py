from PyQt5 import QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt


class LevelButton(QWidget):
    def __init__(self, index: int, levelController):
        super(QWidget, self).__init__()
        self.clicked = QAction()
        self.index = index
        self.levelController = levelController
        self.setAutoFillBackground(True)
        palette = QPalette()
        palette.setColor(self.backgroundRole(), QColor(255, 0, 0)) #215, 220, 255
        self.setPalette(palette)
        self.layout = QHBoxLayout()
        if index != -1:
            label = QLabel("Niveau " + str(index))
            label.setFont(QFont("Arial", 20))
        else:
            label = QLabel("∞")
            label.setFont(QFont("Arial", 60))
        self.layout.addWidget(label, alignment=Qt.AlignCenter)
        self.setLayout(self.layout)
        self.connect()

    def connect(self):
        if self.index == -1:
            self.index = 6
        self.clicked.triggered.connect(lambda: self.levelController.chooseLevel(self.index))

    def mousePressEvent(self, QMouseEvent):
        self.clicked.trigger()

