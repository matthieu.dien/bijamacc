from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from view.levelGui import LevelGui
from view.treeView import TreeView


class DyckWordToBinaryTreeGui(LevelGui):
    def __init__(self, controller):
        super().__init__(controller)

        self.createDyckWordGroupBox()
        self.createTreeView()


        self.layout = QGridLayout()
        self.layout.addLayout(self.menuBarLayout, 0, 0, 1, 2)
        self.layout.addWidget(self.dyckWordGroupBox, 1, 0)
        self.layout.addWidget(self.treeView, 1, 1)
        self.layout.setColumnStretch(0, 1)
        self.layout.setColumnStretch(1, 2)

        self.setLayout(self.layout)


    def createDyckWordGroupBox(self):
        self.dyckWordGroupBox = QGroupBox()
        self.globalDyckWordLayout = QVBoxLayout()
        self.dyckWordLayout = QHBoxLayout()
        self.globalDyckWordLayout.addLayout(self.dyckWordLayout, 1)
        self.dyckWordGroupBox.setLayout(self.globalDyckWordLayout)
        word = self.controller.level.answer.dyckWord
        newWord = ""
        for i in range(len(word)):
            newWord += word[i]
            if i != len(word) - 1:
                newWord += " "
        label = QLabel(newWord)
        label.setFont(QFont("Arial", 25))
        self.dyckWordLayout.addStretch(1)
        self.dyckWordLayout.addWidget(label)
        self.dyckWordLayout.addStretch(1)


    def error(self, index: int, nodeType: str):
        self.changeScore()
        if nodeType == "leaf":
            indexList = 2 * index
        else:
            indexList = 2 * index + 1
        self.treeView.buttons[indexList].error()


    def createTreeView(self):
        self.treeScene = QGraphicsScene()
        self.treeView = TreeView(self.treeScene, controller=self.controller)
        self.treeView.drawTree()



