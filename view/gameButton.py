from PyQt5 import QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt

from model.GameEnum import GameEnum


class GameButton(QWidget):
    def __init__(self, game: GameEnum):
        super(QWidget, self).__init__()
        self.clicked = QAction()
        self.game = game
        self.setAutoFillBackground(True)
        palette = QPalette()
        palette.setColor(self.backgroundRole(), QColor(255, 0, 0)) #215, 220, 255
        self.setPalette(palette)
        self.layout = QHBoxLayout()
        """label = QLabel(self.game.name)
        self.layout.addWidget(label, alignment=Qt.AlignCenter)"""
        self.setLayout(self.layout)
        #self.connect()

    def mousePressEvent(self, QMouseEvent):
        self.clicked.trigger()

