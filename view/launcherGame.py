from PyQt5.QtGui import QFont, QColor
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QPushButton, QGridLayout
from PyQt5.QtCore import Qt
from model.GameEnum import GameEnum
from view.menuButton import MenuButton


def title(game: GameEnum):
    if game == GameEnum.DYCK_WORD_TO_BINARY_TREE:
        return "Jeu: Mot de Dyck - > Arbre binaire"
    elif game == GameEnum.BINARY_TREE_TO_PLANAR_TREE:
        return "Jeu: Arbre binaire - > Arbre planaire"
    elif game == GameEnum.BINARY_TREE_TO_DYCK_WORD:
        return "Jeu: Arbre binaire - > Mot de Dyck"
    elif game == GameEnum.PLANAR_TREE_TO_BINARY_TREE:
        return "Jeu: Arbre planaire - > Arbre binaire"
    elif game == GameEnum.PLANAR_TREE_TO_DYCK_WORD:
        return "Jeu: Arbre planaire - > Mot de Dyck"
    else:
        return "Jeu: Mot de Dyck - > Arbre planaire"


def explaination(game: GameEnum):
    str = "Explications:\n\n"
    if game == GameEnum.DYCK_WORD_TO_BINARY_TREE:
        str += "Il existe une bijection entre les mots de dycks et\nles arbres binaires, c'est à dire que l'on peut " \
               "représenter\nn'importe quel mot de dyck par un arbre binaire et\ninversement.Un mot de dyck est un mot " \
               "correctement\nparenthesé, il contient autant de parenthèses ouvrantes\nque de parenthèses fermantes et " \
               "à chaque endroit où\nl'on se trouve dans le mot, le nombre de parenthèses\nouvrantes et supérieur ou " \
               "égal au nombre de parenthèses\nfermantes.\n\nDans ce jeu, on vous donnera un mot de dyck et vous\ndevrez " \
               "dessiner l'abre binaire qui correspond en faisant\nle moins d'erreurs possible!"
    elif game == GameEnum.BINARY_TREE_TO_PLANAR_TREE:
        str += "Il existe une bijection entre les arbres binaires\net les arbres planaires, c'est à dire que l'on peut\n" \
               "représenter n'importe quel arbre binaire par un arbre\nplanaire et inversement.Un arbre binaire est un " \
               "arbre\ndans lequel chacun de ses noeuds possède 2 fils. Les\nnoeuds d'un arbre planaire possèdent " \
               "un nombre illimité\nde fils." \
               "\n\nDans ce jeu, on vous donnera arbre binaire et vous\ndevrez dessiner l'abre planaire qui correspond " \
               "en faisant\nle moins d'erreurs possible!"
    elif game == GameEnum.BINARY_TREE_TO_DYCK_WORD:
        str += "Il existe une bijection entre les mots de dycks et\nles arbres binaires, c'est à dire que l'on peut " \
               "représenter\nn'importe quel mot de dyck par un arbre binaire et\ninversement.Un mot de dyck est un " \
               "mot correctement\nparenthesé, il contient autant de parenthèses ouvrantes\nque de parenthèses " \
               "fermantes et à chaque endroit où\nl'on se trouve dans le mot, le nombre de parenthèses\nouvrantes " \
               "et supérieur ou égal au nombre de parenthèses\nfermantes.\n\nDans ce jeu, on vous donnera un arbre " \
               "binaire et un\nmot de dyck avec des trous. Vous devrez compléter\nle mot de dyck en faisant le moins " \
               "d'erreurs possible!"
    elif game == GameEnum.PLANAR_TREE_TO_BINARY_TREE:
        str += "Il existe une bijection entre les arbres binaires\net les arbres planaires, c'est à dire que l'on peut\n" \
               "représenter n'importe quel arbre binaire par un arbre\nplanaire et inversement.Un arbre binaire est un " \
               "arbre\ndans lequel chacun de ses noeuds possède 2 fils. Les\nnoeuds d'un arbre planaire possèdent " \
               "un nombre illimité\nde fils." \
               "\n\nDans ce jeu, on vous donnera arbre planaire et vous\ndevrez dessiner l'abre binaire qui correspond " \
               "en faisant\nle moins d'erreurs possible!"
    elif game == GameEnum.DYCK_WORD_TO_PLANAR_TREE:
        str += "Il existe une bijection entre les mots de dycks et\nles arbres planaires, c'est à dire que l'on peut " \
               "représenter\nn'importe quel mot de dyck par un arbre planaire et\ninversement. Un mot de dyck est un mot " \
               "correctement\nparenthesé, il contient autant de parenthèses ouvrantes\nque de parenthèses fermantes et " \
               "à chaque endroit où\nl'on se trouve dans le mot, le nombre de parenthèses\nouvrantes et supérieur ou " \
               "égal au nombre de parenthèses\nfermantes.\n\nDans ce jeu, on vous donnera un mot de dyck et vous\ndevrez " \
               "dessiner l'abre planaire qui correspond en faisant\nle moins d'erreurs possible!"
    else:
        str += "Il existe une bijection entre les mots de dycks et\nles arbres planaires, c'est à dire que l'on peut " \
               "représenter\nn'importe quel mot de dyck par un arbre planaire et\ninversement.Un mot de dyck est un " \
               "mot correctement\nparenthesé, il contient autant de parenthèses ouvrantes\nque de parenthèses " \
               "fermantes et à chaque endroit où\nl'on se trouve dans le mot, le nombre de parenthèses\nouvrantes " \
               "et supérieur ou égal au nombre de parenthèses\nfermantes.\n\nDans ce jeu, on vous donnera un arbre " \
               "planaire et un\nmot de dyck avec des trous. Vous devrez compléter\nle mot de dyck en faisant le moins " \
               "d'erreurs possible!"
    return str


class LauncherGame(QWidget):
    def __init__(self, game: GameEnum, controller):
        super(QWidget, self).__init__()
        self.game = game
        self.controller = controller
        self.titleLabel = QLabel(title(self.game))
        self.titleLabel.setFont(QFont("Arial", 20))
        self.explainationLabel = QLabel(explaination(self.game))
        self.explainationLabel.setFont(QFont("Arial", 12))
        self.explainationLabel.setAlignment(Qt.AlignJustify)
        self.demoButton = QPushButton("Demo")
        self.levelButton = QPushButton("Niveaux")
        self.beginButton = QPushButton("Commencer")

        buttonLayout = QGridLayout()
        buttonLayout.setSpacing(20)
        buttonLayout.addWidget(self.demoButton, 0, 0, alignment=Qt.AlignRight)
        buttonLayout.addWidget(self.beginButton, 0, 1, alignment=Qt.AlignLeft)
        buttonLayout.addWidget(self.levelButton, 1, 0, 1, 2, alignment=Qt.AlignCenter)

        layout = QVBoxLayout()

        menuButton = MenuButton(QColor(52, 73, 94))
        menuButton.clickableObject.clicked.connect(controller.menuController.displayMenu)
        layout.addWidget(menuButton, alignment=Qt.AlignLeft)
        layout.addStretch(1)
        layout.addWidget(self.titleLabel, alignment=Qt.AlignCenter)
        layout.addStretch(2)
        layout.addWidget(self.explainationLabel, alignment=Qt.AlignCenter)
        layout.addStretch(3)
        layout.addLayout(buttonLayout)
        layout.addStretch(2)

        self.setLayout(layout)

def getTexts():
    l = []
    for i in range(GameEnum.__len__()):
        str = explaination(GameEnum(i))
        copy = ""
        cpt = -1
        for j in str:
            cpt += 1
            if j == "\n":
                cpt = 0
            if cpt >= 50 and j == ' ':
                copy += "\\n"
                cpt = 0
            else:
                copy += j
        l += copy
        print(copy + "\n\n\n")


if __name__ == '__main__':
    print(getTexts())

