from PyQt5.QtGui import QFont, QPalette, QColor, QBrush, QPen
from PyQt5.QtWidgets import *

from PyQt5.QtCore import Qt

from view.menuButton import MenuButton
from view.treeView import Clickable


class LevelGui(QWidget):
    def __init__(self, controller):
        super(QWidget, self).__init__()
        self.menuBarLayout = None
        self.controller = controller
        self.color = QColor(52, 73, 94)

        self.shortCutAction = QAction()
        self.shortCutAction.setShortcut("Ctrl+B")
        self.addAction(self.shortCutAction)

        self.createScoreLabel()
        self.createLevelLabel()
        self.menuButton = MenuButton(self.color)
        self.menuButton.clickableObject.clicked.connect(controller.menuController.displayMenu)
        self.levelWidget = LevelWidget(self.color)
        self.levelWidget.clickableObject.clicked.connect(controller.displayLevels)
        self.createMenuBar()
        self.createEndBox()

    def createLevelLabel(self):
        paletteText = QPalette()
        paletteText.setColor(QPalette.Foreground, self.color)
        self.levelLabel = QLabel("Niveau " + str(self.controller.level.index))
        self.levelLabel.setAutoFillBackground(True)
        self.levelLabel.setFont(QFont("Arial", 20))
        self.levelLabel.setPalette(paletteText)

    def createScoreLabel(self):
        paletteNumber = QPalette()
        paletteNumber.setColor(QPalette.Foreground, QColor(24, 106, 59))
        paletteText = QPalette()
        paletteText.setColor(QPalette.Foreground, self.color)
        self.numberPointsLabel = QLabel(str(self.controller.level.score))
        self.numberPointsLabel.setFixedSize(60, 25)
        self.numberPointsLabel.setAutoFillBackground(True)
        self.numberPointsLabel.setFont(QFont("Arial", 20))
        self.numberPointsLabel.setPalette(paletteNumber)
        self.scoreLabel = QLabel("Score : ")
        self.scoreLabel.setFixedSize(105, 25)
        self.scoreLabel.setAutoFillBackground(True)
        self.scoreLabel.setFont(QFont("Arial", 20))
        self.scoreLabel.setPalette(paletteText)

    def changeScore(self):
        palette = QPalette()
        score = self.controller.level.score
        self.numberPointsLabel.setText(str(score))
        if score > 80:
            palette.setColor(QPalette.Foreground, QColor(24, 106, 59))
        elif score > 60:
            palette.setColor(QPalette.Foreground, QColor(34, 153, 84))
        elif score > 40:
            palette.setColor(QPalette.Foreground, QColor(82, 190, 128))
        elif score > 20:
            palette.setColor(QPalette.Foreground, QColor(244, 208, 63))
        elif score > 0:
            palette.setColor(QPalette.Foreground, QColor(230, 126, 34))
        else:
            palette.setColor(QPalette.Foreground, QColor(169, 50, 38))
        self.numberPointsLabel.setPalette(palette)

    def createMenuBar(self):
        self.menuBarLayout = QHBoxLayout()
        self.menuBarLayout.addWidget(self.menuButton, alignment=Qt.AlignLeft)
        self.menuBarLayout.addWidget(self.levelWidget, alignment=Qt.AlignLeft)
        self.menuBarLayout.addStretch(1)
        self.menuBarLayout.addWidget(self.levelLabel, alignment=Qt.AlignCenter)
        self.menuBarLayout.addStretch(1)
        self.menuBarLayout.addWidget(self.scoreLabel, alignment=Qt.AlignRight)
        self.menuBarLayout.addWidget(self.numberPointsLabel, alignment=Qt.AlignRight)


    def createEndBox(self):
        self.msgBox = QMessageBox()
        self.msgBox.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowTitleHint)
        self.okButton = QPushButton("Suivant")
        self.cancelButton = QPushButton("Rejouer")
        self.msgBox.setText("Bravo, vous pouvez passer au niveau suivant!")
        self.msgBox.addButton(self.cancelButton, QMessageBox.NoRole)
        self.msgBox.addButton(self.okButton, QMessageBox.YesRole)
        self.msgBox.setDefaultButton(self.okButton)

    def finish(self):
        self.msgBox.setWindowTitle(str(self.controller.level.score) + " points")
        self.msgBox.exec_()


class LevelWidget(QWidget):
    def __init__(self, color):
        super().__init__()
        self.clickableObject = Clickable()
        layout = QHBoxLayout()
        scene = QGraphicsScene()
        pen = QPen(color)
        for i in range(3):
            for j in range(3):
                square = QGraphicsRectItem(i * 10, j * 10, 5, 5)
                square.setBrush(color)
                square.setPen(pen)
                scene.addItem(square)
        view = QGraphicsView(scene)
        layout.addWidget(view)
        self.setStyleSheet("border: 0px; background: #F2F3F4")
        self.setLayout(layout)
        self.setFixedSize(50, 50)

    def mousePressEvent(self, e):
        self.clickableObject.clicked.emit()
