from PyQt5.QtGui import QPen
from PyQt5.QtWidgets import *

from view.treeView import Clickable


class MenuButton(QWidget):
    def __init__(self, color):
        super().__init__()
        self.clickableObject = Clickable()
        self.color = color
        layout = QHBoxLayout()
        self.scene = QGraphicsScene()
        self.drawPycto()
        view = QGraphicsView(self.scene)
        layout.addWidget(view)
        self.setStyleSheet("border: 0px")
        self.setLayout(layout)
        self.setFixedSize(60, 60)

    def drawPycto(self):
        pen = QPen(self.color)
        pen.setWidth(3)
        unit = 3.5
        self.scene.setSceneRect(0.25 * unit, unit, 7.5 * unit, 7 * unit)
        line1 = QGraphicsLineItem(1 * unit, 7 * unit, 1 * unit, 3 * unit)
        line2 = QGraphicsLineItem(1 * unit, 7 * unit, 3 * unit, 7 * unit)
        line3 = QGraphicsLineItem(3 * unit, 7 * unit, 3 * unit, 4 * unit)
        line4 = QGraphicsLineItem(3 * unit, 4 * unit, 5 * unit, 4 * unit)
        line5 = QGraphicsLineItem(5 * unit, 4 * unit, 5 * unit, 7 * unit)
        line6 = QGraphicsLineItem(5 * unit, 7 * unit, 7 * unit, 7 * unit)
        line7 = QGraphicsLineItem(7 * unit, 7 * unit, 7 * unit, 3 * unit)
        line8 = QGraphicsLineItem(0.25 * unit, 3.75 * unit, 4 * unit, 0 * unit)
        line9 = QGraphicsLineItem(4 * unit, 0 * unit, 7.75 * unit, 3.75 * unit)
        line1.setPen(pen)
        line2.setPen(pen)
        line3.setPen(pen)
        line4.setPen(pen)
        line5.setPen(pen)
        line6.setPen(pen)
        line7.setPen(pen)
        line8.setPen(pen)
        line9.setPen(pen)
        self.scene.addItem(line1)
        self.scene.addItem(line2)
        self.scene.addItem(line3)
        self.scene.addItem(line4)
        self.scene.addItem(line5)
        self.scene.addItem(line6)
        self.scene.addItem(line7)
        self.scene.addItem(line8)
        self.scene.addItem(line9)

    def mousePressEvent(self, e):
        self.clickableObject.clicked.emit()