from math import sqrt, cos, pi, sin

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt

from model.GameEnum import GameEnum
from model.node import Leaf, BinaryNode
from model.tree import Tree
from view.treeView import Clickable


def get_model_tree():
    return Tree(
        BinaryNode(
            BinaryNode(
                Leaf(),
                Leaf()
            ),
            BinaryNode(
                Leaf(),
                BinaryNode(
                    Leaf(),
                    Leaf()
                )
            )
        ))


class MenuWidget(QWidget):
    def __init__(self, controller):
        super(QWidget, self).__init__()
        self.test = 0

        self.layout = QHBoxLayout()
        self.controller = controller

        self.createMenuView()

        self.setStyleSheet("background: white")
        self.layout.addWidget(self.menuView)
        self.setLayout(self.layout)

    def createMenuView(self):
        self.menuScene = QGraphicsScene()
        self.menuView = MenuView(self.menuScene, controller=self.controller)


class MenuView(QGraphicsView):

    def __init__(self, *args, controller=None, **kwargs):
        super(QGraphicsView, self).__init__(*args, **kwargs)
        self.controller = controller
        self.arrows = []
        self.itemsGroup = QGraphicsItemGroup()
        self.scene().addItem(self.itemsGroup)
        self.drawGames()

    def drawGames(self):
        self.scene().removeItem(self.itemsGroup)
        for arrow in self.arrows:
            self.scene().removeItem(arrow)
        self.arrows = []
        self.itemsGroup = QGraphicsItemGroup()
        self.h = self.height()
        self.w = self.width()
        self.c1x = 0.5 * self.w
        self.c1y = 0.25 * self.h
        self.c2x = 0.5 * self.w - self.h / (2 * sqrt(3))
        self.c2y = 0.75 * self.h
        self.c3x = 0.5 * self.w + self.h / (2 * sqrt(3))
        self.c3y = 0.75 * self.h
        self.circleSize = 0.45 * min(self.h, self.w)
        self.drawCircles()
        self.drawBinaryTree()
        self.drawPlanarTree()
        self.drawDyckWord()
        self.drawPlanarTreeToBinaryTreeArrow()
        self.drawPlanarTreeToDyckWordArrow()
        self.drawBinaryTreeToPlanarTreeArrow()
        self.drawBinaryTreeToDyckWordArrow()
        self.drawDyckWordToPlanarTreeArrow()
        self.drawDyckWordToBinaryTreeArrow()

        self.scene().addItem(self.itemsGroup)
        self.scene().setSceneRect(self.itemsGroup.sceneBoundingRect())

    def drawPlanarTreeToBinaryTreeArrow(self):
        beginX = self.c2x + 0.5 * self.circleSize - 0.5 * self.circleSize * sin(pi / 11)
        beginY = self.c2y + 0.5 * self.circleSize - 0.5 * self.circleSize * cos(pi / 11)
        endX = self.c1x + 0.5 * self.circleSize - 0.5 * self.circleSize * cos(pi / 11)
        endY = self.c1y + 0.5 * self.circleSize + 0.5 * self.circleSize * sin(pi / 11)
        xLength = endX - beginX
        yLength = beginY - endY
        p1x = beginX + 0.05 * xLength
        p1y = beginY - 0.5 * yLength
        p2x = beginX + 0.35 * xLength
        p2y = beginY - 0.8 * yLength
        arrow = Arrow(GameEnum.PLANAR_TREE_TO_BINARY_TREE, beginX, beginY, endX, endY, p1x, p1y, p2x, p2y,
                      self.circleSize, self.controller)
        self.arrows.append(arrow)
        self.scene().addItem(arrow)

    def drawPlanarTreeToDyckWordArrow(self):
        beginX = self.c2x + self.circleSize
        beginY = self.c2y + 0.5 * self.circleSize
        endX = self.c3x
        endY = self.c3y + 0.5 * self.circleSize
        xLength = endX - beginX
        p1x = beginX + 0.2 * xLength
        p1y = beginY - 0.07 * self.circleSize
        p2x = beginX + 0.5 * xLength
        p2y = beginY - 0.07 * self.circleSize
        arrow = Arrow(GameEnum.PLANAR_TREE_TO_DYCK_WORD, beginX, beginY, endX, endY, p1x, p1y, p2x, p2y,
                      self.circleSize, self.controller)
        self.arrows.append(arrow)

        self.scene().addItem(arrow)

    def drawBinaryTreeToPlanarTreeArrow(self):
        beginX = self.c1x + 0.5 * self.circleSize - (sin(pi / 5) * 0.5 * self.circleSize)
        beginY = self.c1y + 0.5 * self.circleSize + (cos(pi / 5) * 0.5 * self.circleSize)
        endX = self.c2x + 0.5 * self.circleSize + (sin(pi / 7) * 0.5 * self.circleSize)
        endY = self.c2y + 0.5 * self.circleSize - (cos(pi / 7) * 0.5 * self.circleSize)
        xLength = beginX - endX
        yLength = endY - beginY
        p1x = beginX + 0.35 * xLength
        p1y = beginY + 0.15 * yLength
        p2x = beginX + 0.35 * xLength
        p2y = beginY + 0.25 * yLength
        arrow = Arrow(GameEnum.BINARY_TREE_TO_PLANAR_TREE, beginX, beginY, endX, endY, p1x, p1y, p2x, p2y,
                      self.circleSize, self.controller)
        self.arrows.append(arrow)

        self.scene().addItem(arrow)

    def drawBinaryTreeToDyckWordArrow(self):
        beginX = self.c1x + 0.5 * self.circleSize + 0.5 * self.circleSize * cos(pi / 11)
        beginY = self.c1y + 0.5 * self.circleSize + 0.5 * self.circleSize * sin(pi / 11)
        endX = self.c3x + 0.5 * self.circleSize + 0.5 * self.circleSize * sin(pi / 11)
        endY = self.c3y + 0.5 * self.circleSize - 0.5 * self.circleSize * cos(pi / 11)
        xLength = endX - beginX
        yLength = endY - beginY
        p1x = beginX + 0.5 * xLength
        p1y = beginY + 0.15 * yLength
        p2x = beginX + 0.9 * xLength
        p2y = beginY + 0.45 * yLength
        arrow = Arrow(GameEnum.BINARY_TREE_TO_DYCK_WORD, beginX, beginY, endX, endY, p1x, p1y, p2x, p2y,
                      self.circleSize, self.controller)
        self.arrows.append(arrow)

        self.scene().addItem(arrow)

    def drawDyckWordToPlanarTreeArrow(self):
        beginX = self.c3x + 0.5 * self.circleSize - (sin(pi / 4) * 0.5 * self.circleSize)
        beginY = self.c3y + 0.5 * self.circleSize + (cos(pi / 4) * 0.5 * self.circleSize)
        endX = self.c2x + 0.5 * self.circleSize + (sin(pi / 4) * 0.5 * self.circleSize)
        endY = self.c2y + 0.5 * self.circleSize + (cos(pi / 4) * 0.5 * self.circleSize)
        xLength = beginX - endX
        p1x = beginX - 0.2 * xLength
        p1y = beginY + 0.1 * self.circleSize
        p2x = beginX - 0.6 * xLength
        p2y = beginY + 0.1 * self.circleSize
        arrow = Arrow(GameEnum.DYCK_WORD_TO_PLANAR_TREE, beginX, beginY, endX, endY, p1x, p1y, p2x, p2y,
                      self.circleSize, self.controller)
        self.arrows.append(arrow)
        self.scene().addItem(arrow)

    def drawDyckWordToBinaryTreeArrow(self):
        beginX = self.c3x + 0.5 * self.circleSize - (sin(pi / 7) * 0.5 * self.circleSize)
        beginY = self.c3y + 0.5 * self.circleSize - (cos(pi / 7) * 0.5 * self.circleSize)
        endX = self.c1x + 0.5 * self.circleSize + (sin(pi / 5) * 0.5 * self.circleSize)
        endY = self.c1y + 0.5 * self.circleSize + (cos(pi / 5) * 0.5 * self.circleSize)
        xLength = beginX - endX
        yLength = beginY - endY
        p1x = beginX - 0.6 * xLength
        p1y = beginY - 0.1 * yLength
        p2x = beginX - 1 * xLength
        p2y = beginY - 0.45 * yLength
        arrow = Arrow(GameEnum.DYCK_WORD_TO_BINARY_TREE, beginX, beginY, endX, endY, p1x, p1y, p2x, p2y,
                      self.circleSize, self.controller)
        self.arrows.append(arrow)
        self.scene().addItem(arrow)

    def drawArrow(self, beginX: float, beginY: float, endX: float, endY: float, p1x: float, p1y: float, p2x: float,
                  p2y: float, game: GameEnum):
        arrow = Arrow(game, beginX, beginY, endX, endY, p1x, p1y, p2x, p2y, self.circleSize, self.controller)
        self.arrows.append(arrow)
        self.scene().addItem(arrow)


    def drawCircles(self):
        pen = QPen(QColor(23, 32, 42))
        pen.setWidth(5)
        circle1 = QGraphicsEllipseItem(self.c1x, self.c1y, self.circleSize, self.circleSize)
        circle1.setPen(pen)
        circle1.setBrush(QBrush(QColor(235, 245, 251)))
        circle2 = QGraphicsEllipseItem(self.c2x, self.c2y, self.circleSize, self.circleSize)
        circle2.setPen(pen)
        circle2.setBrush(QBrush(QColor(253, 237, 236)))
        circle3 = QGraphicsEllipseItem(self.c3x, self.c3y, self.circleSize, self.circleSize)
        circle3.setPen(pen)
        circle3.setBrush(QBrush(QColor(209, 242, 235)))
        circle4 = QGraphicsEllipseItem(0.45 * self.w, 0.477 * self.h, 0.68 * self.h, 0.68 * self.h)
        circle5 = QGraphicsEllipseItem(0.512 * self.w, 0.61 * self.h, 0.4 * self.h, 0.4 * self.h)
        x1 = QGraphicsEllipseItem(self.c1x + 0.5 * self.circleSize, self.c1y + 0.5 * self.circleSize, 10, 10)
        x2 = QGraphicsEllipseItem(self.c2x + 0.5 * self.circleSize, self.c2y + 0.5 * self.circleSize, 10, 10)
        x3 = QGraphicsEllipseItem(self.c3x + 0.5 * self.circleSize, self.c3y + 0.5 * self.circleSize, 10, 10)
        self.itemsGroup.addToGroup(circle1)
        self.itemsGroup.addToGroup(circle2)
        self.itemsGroup.addToGroup(circle3)
        """self.itemsGroup.addToGroup(circle4)
        self.itemsGroup.addToGroup(circle5)
        self.itemsGroup.addToGroup(x1)
        self.itemsGroup.addToGroup(x2)
        self.itemsGroup.addToGroup(x3)"""

    def drawDyckWord(self):
        word = get_model_tree().dyckWord
        newWord = ""
        for i in range(len(word)):
            newWord += word[i]
            if i != len(word) - 1:
                newWord += " "
        dyckWord = QGraphicsTextItem(newWord)
        dyckWord.setTextWidth(self.circleSize)
        dyckWord.setX(self.c3x + 0.06 * self.circleSize)
        dyckWord.setY(self.c3y + 0.37 * self.circleSize)
        """dyckWord.setX(0.75 * self.w - 0.95 * self.circleSize)
        dyckWord.setY(0.33 * self.h)"""
        size = int(self.circleSize / 8)
        dyckWord.setFont(QFont("Arial", size))
        self.itemsGroup.addToGroup(dyckWord)

    def drawPlanarTree(self):
        nodePen = QPen(QColor(21, 67, 96))
        branchPen = QPen(QColor(21, 67, 96))
        nodeColor = QColor(245, 183, 177)
        lineWidth = int(0.015 * self.circleSize)
        nodeWidth = 0.12 * self.circleSize
        branchPen.setWidth(lineWidth)
        branchPen.setCapStyle(Qt.RoundCap)
        nodePen.setWidth(int(0.1 * nodeWidth))

        x1 = self.c2x + self.circleSize / 2
        y1 = self.c2y + 0.1 * self.circleSize
        x2 = x1 - 0.25 * self.circleSize
        y2 = y1 + 0.35 * self.circleSize
        x3 = x1
        y3 = y2
        x4 = x1 + 0.25 * self.circleSize
        y4 = y2
        x5 = x2
        y5 = y4 + 0.35 * self.circleSize

        line1 = QGraphicsLineItem(x1, y1, x2, y2)
        line1.setPen(branchPen)
        line2 = QGraphicsLineItem(x1, y1, x3, y3)
        line2.setPen(branchPen)
        line3 = QGraphicsLineItem(x1, y1, x4, y4 - nodeWidth / 2)
        line3.setPen(branchPen)
        line4 = QGraphicsLineItem(x2, y2, x5, y5 - nodeWidth / 2)
        line4.setPen(branchPen)
        self.itemsGroup.addToGroup(line1)
        self.itemsGroup.addToGroup(line2)
        self.itemsGroup.addToGroup(line3)
        self.itemsGroup.addToGroup(line4)
        node1 = QGraphicsEllipseItem(x1 - nodeWidth / 2, y1 - nodeWidth / 2, nodeWidth, nodeWidth)
        node1.setPen(nodePen)
        node1.setBrush(nodeColor)
        node2 = QGraphicsEllipseItem(x2 - nodeWidth / 2, y2 - nodeWidth / 2, nodeWidth, nodeWidth)
        node2.setPen(nodePen)
        node2.setBrush(nodeColor)
        node3 = QGraphicsEllipseItem(x3 - nodeWidth / 2, y3 - nodeWidth / 2, nodeWidth, nodeWidth)
        node3.setPen(nodePen)
        node3.setBrush(nodeColor)
        node4 = QGraphicsEllipseItem(x4 - nodeWidth / 2, y4 - nodeWidth / 2, nodeWidth, nodeWidth)
        node4.setPen(nodePen)
        node4.setBrush(nodeColor)
        node5 = QGraphicsEllipseItem(x5 - nodeWidth / 2, y5 - nodeWidth / 2, nodeWidth, nodeWidth)
        node5.setPen(nodePen)
        node5.setBrush(nodeColor)
        self.itemsGroup.addToGroup(node1)
        self.itemsGroup.addToGroup(node2)
        self.itemsGroup.addToGroup(node3)
        self.itemsGroup.addToGroup(node4)
        self.itemsGroup.addToGroup(node5)

    def drawBinaryTree(self):
        nodePen = QPen(QColor(21, 67, 96))
        branchPen = QPen(QColor(21, 67, 96))
        nodeColor = QColor(169, 204, 227)
        lineWidth = int(0.015 * self.circleSize)
        nodeWidth = 0.12 * self.circleSize
        branchPen.setWidth(lineWidth)
        branchPen.setCapStyle(Qt.RoundCap)
        nodePen.setWidth(int(0.1 * nodeWidth))

        x1 = self.c1x + self.circleSize / 2.35
        y1 = 0.1 * self.h + 0.42 * self.circleSize
        x2 = x1 - 0.19 * self.circleSize
        y2 = y1 + 0.23 * self.circleSize
        x3 = x1 + 0.19 * self.circleSize
        y3 = y2
        x4 = x2 - 0.08 * self.circleSize
        y4 = y2 + 0.23 * self.circleSize
        x5 = x2 + 0.08 * self.circleSize
        y5 = y4
        x6 = x3 - 0.08 * self.circleSize
        y6 = y4
        x7 = x3 + 0.08 * self.circleSize
        y7 = y4
        x8 = x7 - 0.08 * self.circleSize
        y8 = y7 + 0.23 * self.circleSize
        x9 = x7 + 0.08 * self.circleSize
        y9 = y8

        leftLine1 = QGraphicsLineItem(x1, y1, x2, y2)
        leftLine1.setPen(branchPen)
        rightLine1 = QGraphicsLineItem(x1, y1, x3, y3)
        rightLine1.setPen(branchPen)
        leftLine2 = QGraphicsLineItem(x2, y2, x4, y4 - nodeWidth / 2)
        leftLine2.setPen(branchPen)
        rightLine2 = QGraphicsLineItem(x2, y2, x5, y5 - nodeWidth / 2)
        rightLine2.setPen(branchPen)
        leftLine3 = QGraphicsLineItem(x3, y3, x6, y6 - nodeWidth / 2)
        leftLine3.setPen(branchPen)
        rightLine3 = QGraphicsLineItem(x3, y3, x7, y7)
        rightLine3.setPen(branchPen)
        leftLine4 = QGraphicsLineItem(x7, y7, x8, y8 - nodeWidth / 2)
        leftLine4.setPen(branchPen)
        rightLine4 = QGraphicsLineItem(x7, y7, x9, y9 - nodeWidth / 2)
        rightLine4.setPen(branchPen)
        self.itemsGroup.addToGroup(leftLine1)
        self.itemsGroup.addToGroup(rightLine1)
        self.itemsGroup.addToGroup(leftLine2)
        self.itemsGroup.addToGroup(rightLine2)
        self.itemsGroup.addToGroup(leftLine3)
        self.itemsGroup.addToGroup(rightLine3)
        self.itemsGroup.addToGroup(leftLine4)
        self.itemsGroup.addToGroup(rightLine4)
        node1 = QGraphicsEllipseItem(x1 - nodeWidth / 2, y1 - nodeWidth / 2, nodeWidth, nodeWidth)
        node1.setPen(nodePen)
        node1.setBrush(nodeColor)
        node2 = QGraphicsEllipseItem(x2 - nodeWidth / 2, y2 - nodeWidth / 2, nodeWidth, nodeWidth)
        node2.setPen(nodePen)
        node2.setBrush(nodeColor)
        node3 = QGraphicsEllipseItem(x3 - nodeWidth / 2, y3 - nodeWidth / 2, nodeWidth, nodeWidth)
        node3.setPen(nodePen)
        node3.setBrush(nodeColor)
        node4 = QGraphicsEllipseItem(x7 - nodeWidth / 2, y7 - nodeWidth / 2, nodeWidth, nodeWidth)
        node4.setPen(nodePen)
        node4.setBrush(nodeColor)
        leaf1 = QGraphicsRectItem(x4 - nodeWidth / 2, y4 - nodeWidth / 2, nodeWidth, nodeWidth)
        leaf1.setPen(nodePen)
        leaf2 = QGraphicsRectItem(x5 - nodeWidth / 2, y5 - nodeWidth / 2, nodeWidth, nodeWidth)
        leaf2.setPen(nodePen)
        leaf3 = QGraphicsRectItem(x6 - nodeWidth / 2, y6 - nodeWidth / 2, nodeWidth, nodeWidth)
        leaf3.setPen(nodePen)
        leaf4 = QGraphicsRectItem(x8 - nodeWidth / 2, y8 - nodeWidth / 2, nodeWidth, nodeWidth)
        leaf4.setPen(nodePen)
        leaf5 = QGraphicsRectItem(x9 - nodeWidth / 2, y9 - nodeWidth / 2, nodeWidth, nodeWidth)
        leaf5.setPen(nodePen)
        self.itemsGroup.addToGroup(node1)
        self.itemsGroup.addToGroup(node2)
        self.itemsGroup.addToGroup(node3)
        self.itemsGroup.addToGroup(node4)
        self.itemsGroup.addToGroup(leaf1)
        self.itemsGroup.addToGroup(leaf2)
        self.itemsGroup.addToGroup(leaf3)
        self.itemsGroup.addToGroup(leaf4)
        self.itemsGroup.addToGroup(leaf5)

    def resizeEvent(self, e):
        e.accept()
        self.drawGames()


class Arrow(QGraphicsItemGroup):
    def __init__(self, game, beginX: float, beginY: float, endX: float, endY: float, p1x: float, p1y: float, p2x: float,
                  p2y: float, circleSize: float, controller):
        super(QGraphicsItemGroup, self).__init__()
        self.clickableObject = Clickable()
        self.game = game
        self.controller = controller
        self.drawArrow(game, beginX, beginY, endX, endY, p1x, p1y, p2x, p2y, circleSize)
        #self.connect()

    def drawArrow(self, game, beginX: float, beginY: float, endX: float, endY: float, p1x: float, p1y: float, p2x: float,
                  p2y: float, circleSize: float):
        offset = 0.03 * circleSize
        pen = QPen()
        pen.setWidth(3)
        if game == GameEnum.PLANAR_TREE_TO_BINARY_TREE:
            endLineX = endX - 0.1 * circleSize
            endLineY = endY + offset

            line = QGraphicsPathItem()
            line.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX - offset, beginY)
            path.cubicTo(p1x - offset, p1y - offset, p2x - offset, p2y - offset, endLineX, endLineY - offset)
            line.setPath(path)

            line2 = QGraphicsPathItem()
            line2.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX + offset, beginY)
            path.cubicTo(p1x + offset, p1y + offset, p2x + offset, p2y + offset, endLineX, endLineY + 1.5 * offset)
            line2.setPath(path)

            line3 = QGraphicsLineItem(endLineX, endLineY - offset, endLineX, endLineY - 2 * offset)
            line3.setPen(pen)

            line4 = QGraphicsLineItem(endLineX, endLineY + 1.5 * offset, endLineX, endLineY + 2.5 * offset)
            line4.setPen(pen)

            line5 = QGraphicsLineItem(endLineX, endLineY - 2 * offset, endX, endY)
            line5.setPen(pen)

            line6 = QGraphicsLineItem(endLineX, endLineY + 2.5 * offset, endX, endY)
            line6.setPen(pen)

        elif game == GameEnum.BINARY_TREE_TO_DYCK_WORD:
            endLineY = endY - 0.1 * circleSize
            endLineX = endX

            line = QGraphicsPathItem()
            line.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX, beginY - offset)
            path.cubicTo(p1x - offset, p1y - offset, p2x + offset, p2y - offset, endLineX + offset, endLineY - 0.01)
            line.setPath(path)

            line2 = QGraphicsPathItem()
            line2.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX, beginY + offset)
            path.cubicTo(p1x - offset, p1y + offset, p2x - offset, p2y + offset, endLineX - offset, endLineY)
            line2.setPath(path)

            line3 = QGraphicsLineItem(endLineX - offset, endLineY, endLineX - 2 * offset, endLineY)
            line3.setPen(pen)

            line4 = QGraphicsLineItem(endLineX + offset, endLineY, endLineX + 2 * offset, endLineY)
            line4.setPen(pen)

            line5 = QGraphicsLineItem(endLineX - 2 * offset, endLineY, endX, endY)
            line5.setPen(pen)

            line6 = QGraphicsLineItem(endLineX + 2 * offset, endLineY, endX, endY)
            line6.setPen(pen)

        elif game == GameEnum.DYCK_WORD_TO_PLANAR_TREE:
            endLineX = endX + 0.1 * circleSize
            endLineY = endY + offset

            line = QGraphicsPathItem()
            line.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX - offset, beginY - 0.3 * offset)
            path.cubicTo(p1x - offset, p1y - offset, p2x - offset, p2y - offset, endLineX, endLineY - offset)
            line.setPath(path)

            line2 = QGraphicsPathItem()
            line2.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX + 0.7 * offset, beginY + 0.3 * offset)
            path.cubicTo(p1x + offset, p1y + offset, p2x + offset, p2y + offset, endLineX, endLineY + 1.5 * offset)
            line2.setPath(path)

            line3 = QGraphicsLineItem(endLineX, endLineY - offset, endLineX, endLineY - 2 * offset)
            line3.setPen(pen)

            line4 = QGraphicsLineItem(endLineX, endLineY + 1.5 * offset, endLineX, endLineY + 2.5 * offset)
            line4.setPen(pen)

            line5 = QGraphicsLineItem(endLineX, endLineY - 2 * offset, endX, endY)
            line5.setPen(pen)

            line6 = QGraphicsLineItem(endLineX, endLineY + 2.5 * offset, endX, endY)
            line6.setPen(pen)
        elif game == GameEnum.DYCK_WORD_TO_BINARY_TREE:
            endLineX = endX
            endLineY = endY + 0.1 * circleSize

            line = QGraphicsPathItem()
            line.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX - 1.5 * offset, beginY + offset)
            path.cubicTo(p1x - 3 * offset, p1y - offset, p2x - offset, p2y - offset, endLineX - offset, endLineY)
            line.setPath(path)

            line2 = QGraphicsPathItem()
            line2.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX, beginY)
            path.cubicTo(p1x + offset, p1y + offset, p2x + offset, p2y + offset, endLineX + offset, endLineY)
            line2.setPath(path)

            line3 = QGraphicsLineItem(endLineX - offset, endLineY, endLineX - 2 * offset, endLineY)
            line3.setPen(pen)

            line4 = QGraphicsLineItem(endLineX + offset, endLineY, endLineX + 2 * offset, endLineY)
            line4.setPen(pen)

            line5 = QGraphicsLineItem(endLineX - 2 * offset, endLineY, endX, endY)
            line5.setPen(pen)

            line6 = QGraphicsLineItem(endLineX + 2 * offset, endLineY, endX, endY)
            line6.setPen(pen)
        elif game == GameEnum.BINARY_TREE_TO_PLANAR_TREE:
            endLineX = endX + 0.07 * circleSize
            endLineY = endY - 0.1 * circleSize

            line = QGraphicsPathItem()
            line.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX - offset, beginY - offset)
            path.cubicTo(p1x - offset, p1y - offset, p2x, p2y - offset, endLineX - 0.3 * offset, endLineY)
            line.setPath(path)

            line2 = QGraphicsPathItem()
            line2.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX + 1.3 * offset, beginY + 0.7 * offset)
            path.cubicTo(p1x + offset, p1y + offset, p2x + offset, p2y + offset, endLineX + 1.5 * offset,
                         endLineY + offset)
            line2.setPath(path)

            line3 = QGraphicsLineItem(endLineX - 0.3 * offset, endLineY, endLineX - 1.3 * offset, endLineY - 0.5 * offset)
            line3.setPen(pen)

            line4 = QGraphicsLineItem(endLineX + 1.5 * offset, endLineY + offset, endLineX + 2.5 * offset,
                                      endLineY + 1.5 * offset)
            line4.setPen(pen)

            line5 = QGraphicsLineItem(endLineX - 1.3 * offset, endLineY - 0.5 * offset, endX, endY)
            line5.setPen(pen)

            line6 = QGraphicsLineItem(endLineX + 2.5 * offset, endLineY + 1.5 * offset, endX, endY)
            line6.setPen(pen)
        else:
            endLineX = endX - 0.1 * circleSize
            endLineY = endY - 0.03 * circleSize

            line = QGraphicsPathItem()
            line.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX, beginY - offset)
            path.cubicTo(p1x - offset, p1y - offset, p2x + offset, p2y - offset, endLineX, endLineY - offset)
            line.setPath(path)

            line2 = QGraphicsPathItem()
            line2.setPen(pen)
            path = QPainterPath()
            path.moveTo(beginX, beginY + offset)
            path.cubicTo(p1x - offset, p1y + offset, p2x - offset, p2y + offset, endLineX, endLineY + offset)
            line2.setPath(path)

            line3 = QGraphicsLineItem(endLineX, endLineY - offset, endLineX, endLineY - 2 * offset)
            line3.setPen(pen)

            line4 = QGraphicsLineItem(endLineX, endLineY + offset, endLineX, endLineY + 2 * offset)
            line4.setPen(pen)

            line5 = QGraphicsLineItem(endLineX, endLineY - 2 * offset, endX, endY)
            line5.setPen(pen)

            line6 = QGraphicsLineItem(endLineX, endLineY + 2 * offset, endX, endY)
            line6.setPen(pen)

        self.addToGroup(line)
        self.addToGroup(line2)
        self.addToGroup(line3)
        self.addToGroup(line4)
        self.addToGroup(line5)
        self.addToGroup(line6)

    def connect(self):
        if self.game == GameEnum.DYCK_WORD_TO_BINARY_TREE:
            self.clickableObject.clicked.connect(self.controller.on_DyckWordToBinaryTree_Button_Pressed)
        elif self.game == GameEnum.BINARY_TREE_TO_DYCK_WORD:
            self.clickableObject.clicked.connect(self.controller.on_binaryTreeToDyckWord_Button_Pressed)
        elif self.game == GameEnum.BINARY_TREE_TO_PLANAR_TREE:
            self.clickableObject.clicked.connect(self.controller.on_binaryTreeToPlanarTree_Button_Pressed)
        elif self.game == GameEnum.PLANAR_TREE_TO_BINARY_TREE:
            self.clickableObject.clicked.connect(self.controller.on_planarTreeToBinaryTree_Button_Pressed)
        elif self.game == GameEnum.PLANAR_TREE_TO_DYCK_WORD:
            self.clickableObject.clicked.connect(self.controller.on_PlanarTreeToDyckWord_Button_Pressed)
        elif self.game == GameEnum.DYCK_WORD_TO_PLANAR_TREE:
            self.clickableObject.clicked.connect(self.controller.on_DyckWordToPlanarTree_Button_Pressed)

    def mousePressEvent(self, event: 'QGraphicsSceneMouseEvent'):
        self.clickableObject.clicked.emit()
