from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import *

from view.levelGui import LevelGui
from view.treeView import TreeView


class BinaryTreeToPlanarTreeGui(LevelGui):
    def __init__(self, controller):
        super().__init__(controller)

        self.createPlanarTreeView()
        self.createTreeView()

        self.layout = QGridLayout()
        self.layout.addLayout(self.menuBarLayout, 0, 0, 1, 2)
        self.layout.addWidget(self.treeView, 1, 0)
        self.layout.addWidget(self.planarTreeView, 1, 1)

        self.setLayout(self.layout)


    def createPlanarTreeView(self):
        self.planarTreeScene = QGraphicsScene()
        self.planarTreeView = TreeView(self.planarTreeScene, controller=self.controller, root=True)
        self.planarTreeView.drawTree()

    def error(self, index: int, nodeType: str):
        self.changeScore()
        if nodeType == "branch":
            indexList = 2 * index
        else:
            indexList = 2 * index + 1
        self.planarTreeView.buttons[indexList].error()

    def createTreeView(self):
        self.treeScene = QGraphicsScene()
        self.treeView = TreeView(self.treeScene, controller=self.controller, model=True)
        self.treeView.drawTree()
