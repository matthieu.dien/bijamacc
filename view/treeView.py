from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, QObject, pyqtSignal

from model.node import Leaf, EmptyNode, BinaryNode, IncompleteNode, PlanarNode
from model.tree import Tree


class TreeView(QGraphicsView):
    """
    class which contains a level of a game and which is used to draw the BinaryTree of the level
    """

    def __init__(self, *args, controller=None, model=False, root=False, tree=None, **kwargs):
        super(QGraphicsView, self).__init__(*args, **kwargs)
        self.controller = controller
        self.setStyleSheet("background: white")
        self.tree = tree
        self.treeItemsGroup = QGraphicsItemGroup()
        self.scene().addItem(self.treeItemsGroup)
        self.indexChildButton = 0
        self.buttons = []
        self.root = root
        self.model = model
        if self.controller is not None:
            if self.model:
                self.tree = self.controller.level.answer
            else:
                self.tree = self.controller.level.tree

    def resizeEvent(self, e):
        e.accept()
        self.drawTree()

    def drawTree(self):
        """
        this method is used to calculate the parameters of the function which draws the tree (dimensions of the tree)
        and to call it
        """
        h = self.height()
        w = self.width()
        self.scene().removeItem(self.treeItemsGroup)
        for button in self.buttons:
            self.scene().removeItem(button)
        self.buttons = []
        self.indexChildButton = 0
        self.treeItemsGroup = QGraphicsItemGroup()
        if self.controller is not None:
            if self.model:
                self.tree = self.controller.level.answer
            else:
                self.tree = self.controller.level.tree

        if self.root:
            if isinstance(self.tree.root, BinaryNode) or isinstance(self.tree.root, EmptyNode):
                tree = Tree(BinaryNode(self.tree.root, Leaf()))
            else:
                tree = self.tree
        else:
            tree = self.tree

        nodeWidth = 0.085 * h
        lineWidth = 0.1 * nodeWidth
        if tree.root.height == 0:
            heightRatio = (h * 0.8 - nodeWidth)
        else:
            heightRatio = ((h * 0.8 - nodeWidth) / tree.root.height)
        if tree.root.width == 0:
            widthRatio = (w * 0.8 - nodeWidth)
        else:
            widthRatio = ((w * 0.8 - nodeWidth) / tree.root.width)
        self.drawTreeRec(tree.root, nodeWidth, lineWidth, heightRatio, widthRatio, self.root)
        self.scene().addItem(self.treeItemsGroup)

        self.scene().setSceneRect(self.treeItemsGroup.sceneBoundingRect())

    def drawTreeRec(self, tree, nodeWidth, lineWidth, heightRatio, widthRatio, root):
        nodePen = QPen(QColor(21, 67, 96))
        branchPen = QPen(QColor(21, 67, 96))
        if not root:
            if isinstance(tree, PlanarNode):
                nodeColor = QColor(245, 183, 177)
            else:
                nodeColor = QColor(169, 204, 227)
        else:
            nodeColor = QColor(187, 143, 206)
            nodePen.setWidth(0.1 * nodeWidth)
        branchPen.setWidth(lineWidth)
        branchPen.setCapStyle(Qt.RoundCap)
        nodePen.setWidth(0.1 * nodeWidth)

        if isinstance(tree, Leaf):
            leaf = QGraphicsRectItem()
            leaf.setRect(tree.x * widthRatio - nodeWidth / 2, tree.y * heightRatio, nodeWidth, nodeWidth)
            leaf.setPen(nodePen)
            self.treeItemsGroup.addToGroup(leaf)
        elif isinstance(tree, EmptyNode):
            leaf = QGraphicsRectItem()
            leaf.setRect(tree.x * widthRatio, tree.y * heightRatio, nodeWidth, nodeWidth)
            leaf.setOpacity(0)
            self.treeItemsGroup.addToGroup(leaf)
            leafButton = LeafButton(self.controller, self.indexChildButton, 0.1 * nodeWidth)
            nodeButton = NodeButton(self.controller, self.indexChildButton, 0.1 * nodeWidth, nodeColor)
            self.indexChildButton += 1
            leafButton.setRect(tree.x * widthRatio - nodeWidth / 2 - 0.05 * nodeWidth,
                               tree.y * heightRatio + nodeWidth / 4,
                               nodeWidth / 2 - 0.1 * nodeWidth, nodeWidth / 2 - 0.1 * nodeWidth)
            nodeButton.setRect(tree.x * widthRatio + 0.05 * nodeWidth, tree.y * heightRatio + nodeWidth / 4,
                               nodeWidth / 2 - 0.1 * nodeWidth,
                               nodeWidth / 2 - 0.1 * nodeWidth)
            self.buttons.append(leafButton)
            self.buttons.append(nodeButton)
            self.scene().addItem(leafButton)
            self.scene().addItem(nodeButton)
        else:
            if isinstance(tree, BinaryNode):
                leftLine = QGraphicsLineItem()
                rightLine = QGraphicsLineItem()
                leftLine.setLine(tree.x * widthRatio, tree.y * heightRatio + nodeWidth / 2, tree.left.x * widthRatio,
                                 tree.left.y * heightRatio)
                leftLine.setPen(branchPen)
                rightLine.setLine(tree.x * widthRatio, tree.y * heightRatio + nodeWidth / 2, tree.right.x * widthRatio,
                                  tree.right.y * heightRatio)
                rightLine.setPen(branchPen)
                self.treeItemsGroup.addToGroup(leftLine)
                self.treeItemsGroup.addToGroup(rightLine)
                node = QGraphicsEllipseItem()
                node.setRect(tree.x * widthRatio - nodeWidth / 2, tree.y * heightRatio, nodeWidth, nodeWidth)
                node.setPen(nodePen)
                node.setBrush(nodeColor)
                self.treeItemsGroup.addToGroup(node)
                self.drawTreeRec(tree.left, nodeWidth, lineWidth, heightRatio, widthRatio, False)
                self.drawTreeRec(tree.right, nodeWidth, lineWidth, heightRatio, widthRatio, False)
            else:
                if isinstance(tree, IncompleteNode):
                    leaf = QGraphicsRectItem()
                    leaf.setRect(tree.x * widthRatio, tree.y * heightRatio, nodeWidth, nodeWidth)
                    leaf.setOpacity(0)
                    self.treeItemsGroup.addToGroup(leaf)
                    branchButton = BranchButton(self.controller, self.indexChildButton, 0.1 *
                                                nodeWidth, tree.x * widthRatio + 0.05 * nodeWidth, tree.y * heightRatio + nodeWidth / 4,
                    nodeWidth / 2 - 0.1 * nodeWidth)
                    nodeButton = NodeButton(self.controller, self.indexChildButton, 0.1 * nodeWidth, nodeColor)
                    self.indexChildButton += 1
                    nodeButton.setRect(tree.x * widthRatio - nodeWidth / 2 - 0.05 * nodeWidth, tree.y * heightRatio +
                                       nodeWidth / 4, nodeWidth / 2 - 0.1 * nodeWidth, nodeWidth / 2 - 0.1 * nodeWidth)
                    self.buttons.append(branchButton)
                    self.buttons.append(nodeButton)
                    self.scene().addItem(nodeButton)
                    self.scene().addItem(branchButton)
                if len(tree.children) > 0:
                    for child in tree.children:
                        line = QGraphicsLineItem()
                        line.setLine(tree.x * widthRatio, tree.y * heightRatio + nodeWidth / 2, child.x * widthRatio,
                                     child.y * heightRatio)
                        line.setPen(branchPen)
                        self.treeItemsGroup.addToGroup(line)
                    if not isinstance(tree, IncompleteNode):
                        node = QGraphicsEllipseItem()
                        node.setRect(tree.x * widthRatio - nodeWidth / 2, tree.y * heightRatio, nodeWidth, nodeWidth)
                        node.setPen(nodePen)
                        node.setBrush(nodeColor)
                        self.treeItemsGroup.addToGroup(node)
                    for child in tree.children:
                        self.drawTreeRec(child, nodeWidth, lineWidth, heightRatio, widthRatio, False)
                else:
                    if not isinstance(tree, IncompleteNode):
                        node = QGraphicsEllipseItem()
                        node.setRect(tree.x * widthRatio - nodeWidth / 2, tree.y * heightRatio, nodeWidth, nodeWidth)
                        node.setPen(nodePen)
                        node.setBrush(nodeColor)
                        self.treeItemsGroup.addToGroup(node)


class Clickable(QObject):
    clicked = pyqtSignal()

    def __init__(self):
        super(QObject, self).__init__()


class LeafButton(QGraphicsRectItem):
    def __init__(self, controller, buttonIndex, width):
        super(QGraphicsRectItem, self).__init__()
        self.clickableObject = Clickable()
        self.index = buttonIndex
        self.controller = controller
        self.pen = QPen(QColor(21, 67, 96))
        self.pen.setWidth(width)
        self.setPen(self.pen)
        self.connect()

    def error(self):
        self.pen.setColor(QColor(255, 0, 0))
        self.setPen(self.pen)

    def connect(self):
        self.clickableObject.clicked.connect(lambda: self.controller.addNode(self.index, "leaf"))

    def mousePressEvent(self, event: 'QGraphicsSceneMouseEvent'):
        self.clickableObject.clicked.emit()


class NodeButton(QGraphicsEllipseItem):
    def __init__(self, controller, buttonIndex, width, color):
        super(QGraphicsEllipseItem, self).__init__()
        self.clickableObject = Clickable()
        self.index = buttonIndex
        self.controller = controller
        self.setBrush(color)
        self.pen = QPen(QColor(21, 67, 96))
        self.pen.setWidth(width)
        self.setPen(self.pen)
        self.connect()

    def error(self):
        self.pen.setColor(QColor(255, 0, 0))
        self.setPen(self.pen)

    def connect(self):
        self.clickableObject.clicked.connect(lambda: self.controller.addNode(self.index, "node"))

    def mousePressEvent(self, event: 'QGraphicsSceneMouseEvent'):
        self.clickableObject.clicked.emit()


class BranchButton(QGraphicsItemGroup):
    def __init__(self, controller, buttonIndex, width, x, y, nodeWidth):
        super(QGraphicsItemGroup, self).__init__()
        self.clickableObject = Clickable()
        self.nodeType = "branch"
        self.index = buttonIndex
        self.controller = controller
        self.width = width
        self.circlePen = QPen(QColor(40, 180, 99))
        self.circlePen.setWidth(width)
        self.linePen = QPen(QColor(40, 180, 99))
        self.linePen.setWidth(width)
        self.circlePen.setWidth(width)
        self.circle = QGraphicsEllipseItem(x, y, nodeWidth, nodeWidth)
        self.circle.setPen(self.circlePen)
        self.horizontalLine = QGraphicsLineItem(x + width + nodeWidth / 8, y + nodeWidth / 2, x + nodeWidth - width - nodeWidth / 8, y + nodeWidth / 2)
        self.verticalLine = QGraphicsLineItem(x + nodeWidth / 2, y + width + nodeWidth / 8, x + nodeWidth / 2, y + nodeWidth - width - nodeWidth / 8)
        self.horizontalLine.setPen(self.linePen)
        self.verticalLine.setPen(self.linePen)
        self.addToGroup(self.circle)
        self.addToGroup(self.verticalLine)
        self.addToGroup(self.horizontalLine)
        self.connect()

    def error(self):
        self.circlePen.setColor(QColor(255, 0, 0))
        self.linePen.setColor(QColor(255, 0, 0))
        self.circle.setPen(self.circlePen)
        self.horizontalLine.setPen(self.linePen)
        self.verticalLine.setPen(self.linePen)

    def connect(self):
        self.clickableObject.clicked.connect(lambda: self.controller.addNode(self.index, "branch"))

    def mousePressEvent(self, event: 'QGraphicsSceneMouseEvent'):
        self.clickableObject.clicked.emit()
