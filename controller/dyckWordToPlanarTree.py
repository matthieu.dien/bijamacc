from controller.levelController import LevelController
from model.GameEnum import GameEnum
from model.levelDyckWordToBinaryTree import LevelDyckWordToBinaryTree
from model.levelDyckWordToPlanarTree import LevelDyckWordToPlanarTree
from view.dyckWordToBinaryTreeGui import DyckWordToBinaryTreeGui
from view.dyckWordToPlanarTreeGui import DyckWordToPlanarTreeGui
from view.launcherGame import LauncherGame


class DyckWordToPlanarTree(LevelController):
    def __init__(self, menuController):
        super().__init__(menuController)
        self.level = LevelDyckWordToPlanarTree(1)

        self.launcher = LauncherGame(GameEnum.DYCK_WORD_TO_PLANAR_TREE, self)
        self.launcher.levelButton.clicked.connect(self.displayLevels)
        self.launcher.beginButton.clicked.connect(self.launchLevel)
        self.launcher.demoButton.clicked.connect(self.demo)

        self.menuController.menu.displayLauncher(self.launcher)

    def launchLevel(self):
        self.levelGui = DyckWordToPlanarTreeGui(self)
        self.levelGui.okButton.clicked.connect(self.nextLevel)
        self.levelGui.cancelButton.clicked.connect(self.sameLevel)
        self.levelGui.shortCutAction.triggered.connect(self.nextLevel)
        self.menuController.menu.displayLevel(self.levelGui)

    def chooseLevel(self, index: int):
        self.level = LevelDyckWordToPlanarTree(index)
        self.launchLevel()

    def addNode(self, index: int, nodeType: str):
        if self.level.checkTree(index, nodeType):
            self.levelGui.treeView.drawTree()
            if self.level.finished():
                self.levelGui.finish()
        else:
            self.level.error()
            self.levelGui.error(index, nodeType)









