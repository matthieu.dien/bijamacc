class LevelController:
    def __init__(self, menuController):
        self.levelGui = None
        self.level = None
        self.menuController = menuController
        self.launcher = None

    def launchLevel(self):
        pass

    def displayLevels(self):
        self.menuController.displayLevels(5, self)


    def demo(self):
        print("voici la demo...")
        self.launchLevel()

    def nextLevel(self):
        print(self.level)
        self.level = self.level.nextLevel()
        self.launchLevel()

    def sameLevel(self):
        self.level.initialize()
        self.launchLevel()

    def chooseLevel(self, index: int):
        pass
