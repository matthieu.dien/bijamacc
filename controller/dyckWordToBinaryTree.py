from controller.levelController import LevelController
from model.GameEnum import GameEnum
from model.levelDyckWordToBinaryTree import LevelDyckWordToBinaryTree
from view.dyckWordToBinaryTreeGui import DyckWordToBinaryTreeGui
from view.launcherGame import LauncherGame


class DyckWordToBinaryTree(LevelController):
    def __init__(self, menuController):
        super().__init__(menuController)
        self.level = LevelDyckWordToBinaryTree(1)

        self.launcher = LauncherGame(GameEnum.DYCK_WORD_TO_BINARY_TREE, self)
        self.launcher.levelButton.clicked.connect(self.displayLevels)
        self.launcher.beginButton.clicked.connect(self.launchLevel)
        self.launcher.demoButton.clicked.connect(self.demo)

        self.menuController.menu.displayLauncher(self.launcher)

    def chooseLevel(self, index: int):
        self.level = LevelDyckWordToBinaryTree(index)
        self.launchLevel()

    def launchLevel(self):
        self.levelGui = DyckWordToBinaryTreeGui(self)
        self.levelGui.okButton.clicked.connect(self.nextLevel)
        self.levelGui.cancelButton.clicked.connect(self.sameLevel)
        self.levelGui.shortCutAction.triggered.connect(self.nextLevel)
        self.menuController.menu.displayLevel(self.levelGui)

    def addNode(self, index: int, nodeType: str):
        if self.level.checkTree(index, nodeType):
            self.levelGui.treeView.drawTree()
            if self.level.finished():
                self.levelGui.finish()
        else:
            self.level.error()
            self.levelGui.error(index, nodeType)









