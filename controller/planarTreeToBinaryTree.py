from controller.levelController import LevelController
from model.GameEnum import GameEnum
from model.levelPlanarTreeToBinaryTree import LevelPlanarTreeToBinaryTree
from view.launcherGame import LauncherGame
from view.planarTreeToBinaryTreeGui import PlanarTreeToBinaryTreeGui


class PlanarTreeToBinaryTree(LevelController):
    def __init__(self, menuController):
        super().__init__(menuController)

        self.level = LevelPlanarTreeToBinaryTree(1)

        self.launcher = LauncherGame(GameEnum.PLANAR_TREE_TO_BINARY_TREE, self)
        self.launcher.levelButton.clicked.connect(self.displayLevels)
        self.launcher.beginButton.clicked.connect(self.launchLevel)
        self.launcher.demoButton.clicked.connect(self.demo)

        self.menuController.menu.displayLauncher(self.launcher)

    def launchLevel(self):
        self.levelGui = PlanarTreeToBinaryTreeGui(self)
        self.levelGui.okButton.clicked.connect(self.nextLevel)
        self.levelGui.cancelButton.clicked.connect(self.sameLevel)
        self.levelGui.shortCutAction.triggered.connect(self.nextLevel)
        self.menuController.menu.displayLevel(self.levelGui)

    def chooseLevel(self, index: int):
        self.level = LevelPlanarTreeToBinaryTree(index)
        self.launchLevel()

    def addNode(self, index: int, nodeType: str):
        if self.level.checkTree(index, nodeType):
            self.levelGui.treeView.drawTree()
            if self.level.finished():
                self.levelGui.finish()
        else:
            self.level.error()
            self.levelGui.error(index, nodeType)









