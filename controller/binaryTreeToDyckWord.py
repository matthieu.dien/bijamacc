from controller.levelController import LevelController
from model.GameEnum import GameEnum
from model.levelBinarytreetodyckword import LevelBinaryTreeToDyckWord
from view.binaryTreeToDyckWordGui import BinaryTreeToDyckWordGui
from view.launcherGame import LauncherGame


class BinaryTreeToDyckWord(LevelController):
    def __init__(self, menuController):
        super().__init__(menuController)
        self.level = LevelBinaryTreeToDyckWord(1)

        self.launcher = LauncherGame(GameEnum.BINARY_TREE_TO_DYCK_WORD, self)
        self.launcher.levelButton.clicked.connect(self.displayLevels)
        self.launcher.beginButton.clicked.connect(self.launchLevel)
        self.launcher.demoButton.clicked.connect(self.demo)

        self.menuController.menu.displayLauncher(self.launcher)

    def launchLevel(self):
        self.levelGui = BinaryTreeToDyckWordGui(self)
        self.levelGui.okButton.clicked.connect(self.nextLevel)
        self.levelGui.cancelButton.clicked.connect(self.sameLevel)
        self.levelGui.shortCutAction.triggered.connect(self.nextLevel)
        self.menuController.menu.displayLevel(self.levelGui)

    def chooseLevel(self, index: int):
        self.level = LevelBinaryTreeToDyckWord(index)
        self.launchLevel()









