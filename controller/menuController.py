from controller.binaryTreeToDyckWord import BinaryTreeToDyckWord
from controller.binaryTreeToPlanarTree import BinaryTreeToPlanarTree
from controller.dyckWordToPlanarTree import DyckWordToPlanarTree
from controller.planarTreeToBinaryTree import PlanarTreeToBinaryTree
from controller.planarTreeToDyckWord import PlanarTreeToDyckWord
from model.GameEnum import GameEnum
from controller.dyckWordToBinaryTree import DyckWordToBinaryTree
from view.menu import Menu


class MenuController:
    def __init__(self):
        self.levelGui = None
        self.level = None
        self.menu = Menu(self)
        self.game = None
        self.displayMenu()

    def launchApplication(self):
        self.menu.window.show()

    def displayMenu(self):
        self.menu.displayGames()
        for gameButton in self.menu.gameButtons:
            if gameButton.game == GameEnum.DYCK_WORD_TO_BINARY_TREE:
                gameButton.clicked.triggered.connect(self.on_DyckWordToBinaryTree_Button_Pressed)
            elif gameButton.game == GameEnum.BINARY_TREE_TO_DYCK_WORD:
                gameButton.clicked.triggered.connect(self.on_binaryTreeToDyckWord_Button_Pressed)
            elif gameButton.game == GameEnum.BINARY_TREE_TO_PLANAR_TREE:
                gameButton.clicked.triggered.connect(self.on_binaryTreeToPlanarTree_Button_Pressed)
            elif gameButton.game == GameEnum.PLANAR_TREE_TO_BINARY_TREE:
                gameButton.clicked.triggered.connect(self.on_planarTreeToBinaryTree_Button_Pressed)
            elif gameButton.game == GameEnum.PLANAR_TREE_TO_DYCK_WORD:
                gameButton.clicked.triggered.connect(self.on_PlanarTreeToDyckWord_Button_Pressed)
            elif gameButton.game == GameEnum.DYCK_WORD_TO_PLANAR_TREE:
                gameButton.clicked.triggered.connect(self.on_DyckWordToPlanarTree_Button_Pressed)

    """def displayMenu(self):
        self.menu.displayGames()
        for arrow in self.menu.menuWidget.menuView.arrows:
            if arrow.game == GameEnum.DYCK_WORD_TO_BINARY_TREE:
                arrow.clickableObject.clicked.connect(self.on_DyckWordToBinaryTree_Button_Pressed)
            elif arrow.game == GameEnum.BINARY_TREE_TO_DYCK_WORD:
                arrow.clickableObject.clicked.connect(self.on_binaryTreeToDyckWord_Button_Pressed)
            elif arrow.game == GameEnum.BINARY_TREE_TO_PLANAR_TREE:
                arrow.clickableObject.clicked.connect(self.on_binaryTreeToPlanarTree_Button_Pressed)
            elif arrow.game == GameEnum.PLANAR_TREE_TO_BINARY_TREE:
                arrow.clickableObject.clicked.connect(self.on_planarTreeToBinaryTree_Button_Pressed)
            elif arrow.game == GameEnum.PLANAR_TREE_TO_DYCK_WORD:
                arrow.clickableObject.clicked.connect(self.on_PlanarTreeToDyckWord_Button_Pressed)
            elif arrow.game == GameEnum.DYCK_WORD_TO_PLANAR_TREE:
                arrow.clickableObject.clicked.connect(self.on_DyckWordToPlanarTree_Button_Pressed)"""

    def displayLevels(self, nbLevel, levelController):
        self.menu.displayLevels(nbLevel, levelController)

    def on_DyckWordToBinaryTree_Button_Pressed(self):
        self.game = DyckWordToBinaryTree(self)

    def on_binaryTreeToDyckWord_Button_Pressed(self):
        self.game = BinaryTreeToDyckWord(self)

    def on_binaryTreeToPlanarTree_Button_Pressed(self):
        self.game = BinaryTreeToPlanarTree(self)

    def on_planarTreeToBinaryTree_Button_Pressed(self):
        self.game = PlanarTreeToBinaryTree(self)

    def on_PlanarTreeToDyckWord_Button_Pressed(self):
        self.game = PlanarTreeToDyckWord(self)

    def on_DyckWordToPlanarTree_Button_Pressed(self):
        self.game = DyckWordToPlanarTree(self)


