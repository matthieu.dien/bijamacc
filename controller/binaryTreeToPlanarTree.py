from controller.levelController import LevelController
from model.GameEnum import GameEnum
from model.levelBinaryTreeToPlanarTree import LevelBinaryTreeToPlanarTree
from view.binaryTreeToPlanarTreeGui import BinaryTreeToPlanarTreeGui
from view.launcherGame import LauncherGame


class BinaryTreeToPlanarTree(LevelController):
    def __init__(self, menuController):
        super().__init__(menuController)

        self.level = LevelBinaryTreeToPlanarTree(1)

        self.launcher = LauncherGame(GameEnum.BINARY_TREE_TO_PLANAR_TREE, self)
        self.launcher.levelButton.clicked.connect(self.displayLevels)
        self.launcher.beginButton.clicked.connect(self.launchLevel)
        self.launcher.demoButton.clicked.connect(self.demo)

        self.menuController.menu.displayLauncher(self.launcher)

    def launchLevel(self):
        self.levelGui = BinaryTreeToPlanarTreeGui(self)
        self.levelGui.okButton.clicked.connect(self.nextLevel)
        self.levelGui.cancelButton.clicked.connect(self.sameLevel)
        self.levelGui.shortCutAction.triggered.connect(self.nextLevel)
        self.menuController.menu.displayLevel(self.levelGui)

    def chooseLevel(self, index: int):
        self.level = LevelBinaryTreeToPlanarTree(index)
        self.launchLevel()

    def addNode(self, index: int, nodeType: str):
        if self.level.checkTree(index, nodeType):
            self.levelGui.planarTreeView.drawTree()
            if self.level.finished():
                self.levelGui.finish()
        else:
            self.level.error()
            self.levelGui.error(index, nodeType)









