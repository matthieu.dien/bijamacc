from controller.levelController import LevelController
from model.GameEnum import GameEnum
from model.levelPlanarTreeToDyckWord import LevelPlanarTreeToDyckWord
from view.launcherGame import LauncherGame
from view.planarTreeToDyckWordGui import PlanarTreeToDyckWordGui


class PlanarTreeToDyckWord(LevelController):
    def __init__(self, menuController):
        super().__init__(menuController)
        self.level = LevelPlanarTreeToDyckWord(1)

        self.launcher = LauncherGame(GameEnum.PLANAR_TREE_TO_DYCK_WORD, self)
        self.launcher.levelButton.clicked.connect(self.displayLevels)
        self.launcher.beginButton.clicked.connect(self.launchLevel)
        self.launcher.demoButton.clicked.connect(self.demo)

        self.menuController.menu.displayLauncher(self.launcher)

    def launchLevel(self):
        self.levelGui = PlanarTreeToDyckWordGui(self)
        self.levelGui.okButton.clicked.connect(self.nextLevel)
        self.levelGui.cancelButton.clicked.connect(self.sameLevel)
        self.levelGui.shortCutAction.triggered.connect(self.nextLevel)
        self.menuController.menu.displayLevel(self.levelGui)

    def chooseLevel(self, index: int):
        self.level = LevelPlanarTreeToDyckWord(index)
        self.launchLevel()









